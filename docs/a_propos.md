---
hide:
  - navigation
  - toc
---
# À propos

## Collectif Traduction-Zotero-fr

Notre collectif est un groupe de traduction du logiciel [Zotero](https://zotero.org) en français.  
Ces traductions concernent :

* l'interface du logiciel via le [Transifex géré par Zotero](https://www.transifex.com/zotero/),
* la documentation via le [Framagit Documentation Zotero Francophone](https://framagit.org/zotero-fr/zotero-fr.frama.io),
* la locale FR de CSL via le [GitHub du blog Zotero francophone](https://github.com/zfrancophone/locales),
* autres : module complémentaire [Zutilo](https://github.com/wshanks/Zutilo).

Nous sommes ouverts à toutes les contributions, [contactez-nous](mailto:contact@zotero-fr.org) !

### Membres du collectif : 

* [Nicolas Chachereau](https://nchachereau.ch/) - Université de Lausanne, 
* [Frédérique Flamerie](https://cv.archives-ouvertes.fr/frederique-flamerie) - Université de Bordeaux, 
* [Raphaël Grolimund](https://people.hes-so.ch/fr/profile/raphael.grolimun) - Haute école de gestion de Genève, 
* [Rodolphe Lemétayer](https://www.linkedin.com/in/rodolphe-lem%C3%A9tayer-14bb8a46/) - Campus HEP Lyon & Framasoft,
* [Alain Marois](https://cv.archives-ouvertes.fr/alain-marois) - CIHAM UMR 5648,
* [Pascal Martinolli](http://orcid.org/0000-0003-0122-5300) - Université de Montréal,
* [Laure Mellifluo](http://www.linkedin.com/in/mellifluo) - Université de Genève.

Le collectif est en grande partie composé de rédacteurs du [blog Zotero francophone](https://zotero.hypotheses.org/).

## Remerciements

Un grand merci à :

* La [Corporation for Digital Scholarship](https://digitalscholar.org/) pour l'autorisation d'utiliser le nom et le logo Zotero pour cette documentation non-officielle,
* L'[association Framasoft](https://framasoft.org) pour les conseils de ses membres sur le choix du logiciel à utiliser et la façon de convertir les contenus de l'ancienne documentation francophone en markdown, ainsi que pour ses services libres (Framagit et Matomo)
* [Numahell](https://framagit.org/numahell), pour sa maîtrise des expressions régulières, qui nous a fait gagner un temps précieux dans la reprise du contenu.