---
hide:
  - navigation
---
# Guide rapide

<style>
img.QSGimg {
  float: left;
  margin: 0 15px 10px 0;
  max-width: 180px;
}

@media (max-width: 450px) { 
    img.QSGimg {
        float: none;
        display: block;
        margin: 1em auto;
        max-width: ;
    }
 }

h2,h3{
  clear:left;
}

</style>


*Consulter cette page dans la documentation officielle de Zotero : [Quick start guide](https://www.zotero.org/support/quick_start_guide) - dernière mise à jour de la traduction : 2023-02-28*
{ data-search-exclude }

## Qu'est-ce que Zotero ?

Zotero [zoh-TAIR-oh] est un logiciel de gestion bibliographique libre, gratuit et multiplateforme (Windows, Linux, Mac OS). Simple d'utilisation, il vous aide à collecter, organiser, citer et partager vos sources.

Lisez la suite pour un aperçu des fonctionnalités et possibilités de Zotero.

## Les fondamentaux

### Comment installer Zotero ?

Consultez les [instructions d'installation](./installation.md).

### Comment ouvrir Zotero?

![Icône Zotero pour ouvrir l'application depuis le menu de démarrage de son ordinateur](./images/how-to-open.png){class="QSGimg"}

Zotero s'exécute comme n'importe quel autre logiciel de votre ordinateur. 

### Que peut-on faire avec Zotero ?

![Aperçu d'un document Zotero, de sa note et de ses pièces jointes](./images/intuitive_interface_FR.png){class="QSGimg"}

Zotero est - dans son utilisation la plus basique - un logiciel de gestion bibliographique. Il est conçu pour stocker, gérer et citer des références bibliographiques, telles que des ouvrages et des articles. Dans Zotero, chacune de ces références constitue un "document". Plus largement, Zotero est un puissant outil pour collecter et organiser les sources et informations de votre recherche.

### La fenêtre principale

-   Dans le panneau de gauche, vous naviguez entre vos bibliothèques et à l'intérieur de ces dernières. Vous voyez affichés dans ce panneau les collections, marqueurs et recherches enregistrées que vous créez dans chacune de vos bibliothèques.

-   Le panneau central correspond à la liste des documents collectés et enregistrés dans Zotero. Vous avez ici un affichage dynamique en fonction de la vue (panneau de gauche) dans laquelle vous vous trouvez. Vous pouvez [trier](./sorting.md) les documents par auteur, titre, date, etc.

-   Le panneau de droite, le panneau du document, affiche les détails du document en cours de sélection dans le panneau central et vous permet d'effectuer différentes actions via chacun des onglets. Vous modifiez par exemple les informations bibliographiques dans l'onglet "Info". 

![La fenêtre principale de Zotero et ses 3 panneaux](./images/fenetre_principale_FR.png){class=imgcenter}

### Quels types de documents peuvent-ils être ajoutés dans Zotero ?

![Les différents types de documents](./images/item_types_FR.png){class="QSGimg"}

Chaque document contient différentes données bibliographiques, en fonction du type de document dont il s'agit. Les documents peuvent être de tout type, des ouvrages, articles et rapports, aux pages web, illustrations, films, lettres, manuscrits, enregistrements sonores, textes de lois, jugements, parmi bien d'autres.

### Que puis-je faire avec les documents ?

![Données bibliographiques d'une notice](./images/item_metadata_FR.png){class="QSGimg"}

Les documents apparaissent dans le panneau central de l'interface de Zotero. Les données bibliographiques pour la notice sélectionnée apparaissent dans le panneau de droite. Elles comportent les titre, auteurs, maison d'édition, date, nombre de pages, et toute autre donnée nécessaire pour la citation du document.

## Organiser

### Les collections

![Glisser un document dans une collection](./images/collections_FR.png){class="QSGimg"}

Dans le panneau de gauche s'affiche "Ma bibliothèque", qui contient tous les documents de votre bibliothèque. Faites un clic-droit sur "Ma bibliothèque" ou cliquez sur le bouton "Nouvelle collection…" ![bouton Nouvelle collection](./images/add_collection.png) au-dessus du panneau gauche pour créer une nouvelle [collection](./collections_and_tags.md#les-collections), un dossier dans lequel les documents concernant un projet ou une thématique spécifique peuvent être placés. Considérez les collections comme des listes de lecture dans un lecteur de musique : dans les collections, les documents sont des alias (ou "liens") vers l'unique exemplaire de chaque document dans votre bibliothèque. Un même document peut appartenir à plusieurs collections à la fois.

### Les marqueurs

![Liste de marqueurs](./images/tags_FR.png){class="QSGimg"}

Des [marqueurs (tags)](./collections_and_tags.md#les-marqueurs) peuvent être associés aux documents. Les intitulés des marqueurs sont choisis par l'utilisateur. À un document peuvent être associés autant de marqueurs que nécessaire. Les marqueurs sont créés dans la colonne de droite, dans l'onglet "Marqueurs" de n'importe quel document. Ils sont renommés ou supprimés dans le sélecteur de marqueurs, en bas de la colonne de gauche. Jusqu'à 9 marqueurs peuvent se voir attribuer une couleur. Les marqueurs colorés sont facilement visibles dans la liste des documents et peuvent être rapidement ajoutés ou retirés à l'aide des touches numériques de votre clavier. 

### Les recherches

![Fenêtre de recherche rapide du panneau central de Zotero](./images/searches_FR.png){class="QSGimg"}

La [recherche rapide](./searching.md#recherche-rapide) est directement accessible depuis la barre d'outils Zotero et affiche les documents dont les données bibliographiques, les marqueurs ou le contenu du texte intégral des pièces jointes correspondent aux termes saisis.
En cliquant sur l'icône loupe à gauche de la recherche rapide vous pouvez ouvrir la fenêtre de [Recherche avancée](./searching.md#recherche-avancée), qui permet d'effectuer des recherches plus complexes ou plus précises.

### Les recherches enregistrées

![Recherche enregistrée, affichée dans le panneau de gauche de la fenêtre Zotero](./images/saved_searches_FR.png){class="QSGimg"}

Les recherches avancées peuvent être enregistrées dans la colonne de gauche. Les [recherches enregistrées](./searching.md#recherches-enregistrées) fonctionnent comme les collections, mais elles sont automatiquement mises à jour avec les nouveaux documents correspondant aux critères de recherche.

## Collecter

### Les fichiers joints

![Différents types de fichiers joints : capture de page web, fichiers PDF et fichier PNG](./images/attachments_FR.png){class="QSGimg"}

Les documents peuvent avoir des notes, des fichiers et des liens qui leur sont attachés. Ces [fichiers joints](./attaching_files.md) sont visibles dans le panneau central, sous leur document parent. Les fichiers joints peuvent être affichés ou masqués en cliquant sur la flèche à côté de leur document parent.

### Les notes

![Note avec texte enrichi](./images/notes_FR.png){class="QSGimg"}

Des [notes en texte enrichi](./notes.md) peuvent être attachées à n'importe quel document en utilisant l'onglet "Notes" dans le panneau de droite. Elles peuvent être modifiées dans le panneau de droite ou dans une fenêtre dédiée. Cliquez sur le bouton "Nouvelle note" ( ![Bouton Nouvelle note](./images/note_add.png) ) dans la barre d'outils pour créer une note sans l'attacher à un document.

### Les fichiers

![Fichier joint au format .xlsx, attaché à un document](./images/files_FR.png){class="QSGimg"}

N'importe quel type de fichier peut être [attaché](./attaching_files.md) à un document. Il est possible d'ajouter des fichiers avec le bouton "Ajouter une pièce jointe" ( ![Bouton Ajouter une pièce jointe](./images/add_file_button.png) ), en faisant un clic-droit sur un document existant ou en faisant un glisser-déposer. Les fichiers n'ont pas besoin d'être attachés à un document existant. Ils peuvent simplement être ajoutés à votre bibliothèque. Les fichiers peuvent aussi être téléchargés automatiquement quand vous collectez des documents en utilisant le [connecteur Zotero de votre navigateur](#collecter-des-documents).

### Les liens et captures de pages web

![Capture de page web, attachée à un document de type "page web"](./images/snapshots_links_FR.png){class="QSGimg"}

[Des pages web](./attaching_files.md#captures-de-pages-web) peuvent être attachées à n'importe quel document, en tant que lien ou capture de page web. Un lien ouvre juste la page web en ligne. Zotero peut aussi enregistrer la capture d'une page web. Une capture est la copie stockée localement d'une page web, dans le même état que lorsque cette dernière a été enregistrée. Les captures de page web sont disponibles sans connexion Internet.

### Collecter des documents

![Bouton "Save to Zotero" dans un navigateur internet](./images/capture.png){class="QSGimg"}

Avec le connecteur Zotero pour Chrome, Firefox, Edge ou Safari, il est facile de créer de nouveaux documents à partir d'informations disponibles sur Internet. En un seul clic, Zotero peut automatiquement créer un document du type approprié, compléter les données bibliographiques, télécharger s'il est disponible le fichier PDF de texte intégral et ajouter les liens utiles (par exemple le lien vers une notice PubMed) ou des fichiers de données supplémentaires.

### Collecter un ou plusieurs documents

![Bouton "Save to Zotero" dans un navigateur internet : icône "Dossier"](./images/multiple_capture.png){class="QSGimg"}

Si [l'icône d'enregistrement](./adding_items_to_zotero.md) est un livre, un article, une image, etc., cliquer sur cette icône ajoutera le document à la collection en cours de sélection dans Zotero. Si l'icône d'enregistrement est un dossier, la page Web contient plusieurs documents. En cliquant sur l'icône, vous ouvrez une boîte de dialogue à partir de laquelle les documents peuvent être sélectionnés et enregistrés dans Zotero.

### Convertisseurs

![Bouton "Save to Zotero" dans un navigateur internet : icône "Livre"](./images/save_button_book.png){class="QSGimg"}

Zotero utilise des morceaux de code appelés [convertisseurs](./translators.md) pour reconnaître les informations sur les pages Web. Il existe des convertisseurs génériques qui fonctionnent avec de nombreux sites, et des convertisseurs écrits pour des sites individuels. Si un site que vous utilisez ne dispose pas d'un convertisseur, n'hésitez pas à en demander un sur les [forums de Zotero](https://www.zotero.org/forum). 

### Enregistrer une page web

![Bouton "Save to Zotero" dans un navigateur internet : icône "Page web"](./images/save_button_webpage.png){class="QSGimg"}

Si le connecteur Zotero ne reconnaît pas les informations présentes sur une page web, vous pouvez tout de même cliquer sur le bouton d'enregistrement dans la barre d'outils de votre navigateur pour enregistrer la page en tant que document de type [Page Web](./attaching_files.md#captures-de-pages-web) avec une capture attachée. Bien que cela enregistre les métadonnées basiques (titre, URL, date de consultation), il sera éventuellement nécessaire d'ajouter manuellement d'autres informations à la notice.

### Ajouter un document par son identifiant

![Fenêtre d'ajout d'un document par son identifiant, dans le panneau central de la fenêtre de Zotero](./images/identifier_FR.png){class="QSGimg"}

Zotero peut [ajouter des documents automatiquement](./adding_items_to_zotero.md#ajouter-un-document-par-son-identifiant) en utilisant leur numéro ISBN (International Standard Book Number), leur DOI (Digital Object Identifier), leur identifiant PubMed, arXiv ou leur bibcode ADS. Cliquez sur "Ajouter un document par son identifiant" dans la barre d'outils Zotero, saisissez l'identifiant et cliquez sur OK. Il est également possible de copier-coller ou de saisir une liste d'identifiants en une seule fois (appuyez sur `Maj+Entrée` pour obtenir un champ de saisie plus grand).

### Les flux RSS

![Flux RSS](./images/feeds_FR.png){class="QSGimg"}

Abonnez-vous aux [flux RSS](./feeds.md) de vos revues ou sites web préférés pour vous tenir au courant des dernières recherches. Accédez à la page web de l'article ou enregistrez les articles dans votre bibliothèque d'un seul clic. 

### Ajouter manuellement des documents

![Activation du bouton "Nouveau document" pour ajouter manuellement un document](./images/add_item_FR.png){class="QSGimg"}

Les documents peuvent être [ajoutés manuellement](./adding_items_to_zotero.md#ajouter-manuellement-des-documents) en cliquant sur ​​le bouton "Nouveau document" (![Nouveau document](./images/add.png)) dans la barre d'outils de Zotero, puis en sélectionnant le type de document approprié. Les métadonnées peuvent ensuite être ajoutées manuellement dans la colonne de droite. Si d'une façon générale vous devez éviter la saisie manuelle, cette option est particulièrement adaptée pour l'ajout de sources primaires qui ne sont pas disponibles en ligne.


## Citer

### Citer des documents

![Listes de styles de citation](./images/styles.png){class="QSGimg"}

Zotero utilise le langage CSL (Citation Style Language) pour mettre en forme correctement les citations, selon différents styles bibliographiques. Zotero prend en charge les principaux [styles](./styles.md) (Chicago, MLA, APA, Vancouver, etc.), ainsi que de très nombreux styles spécifiques.

### L'intégration aux logiciels de traitements de texte

![Barre d'outils Zotero dans un logiciel de traitement de texte](./images/word_integration_tab_FR.png){class="QSGimg"}

Les [modules complémentaires de Zotero pour Word, LibreOffice et Google Docs](./word_processor_integration.md) permettent d'insérer des citations de sa bibliothèque directement dans son traitement de texte. Cela permet de citer plusieurs pages ou sources ou de personnaliser les citations en un tournemain. Les citations dans le texte, les notes de bas de page et les notes de fin sont toutes prises en charge. Grâce à des [modules complémentaires développés par la communauté](https://www.zotero.org/support/plugins#latex_tex_and_plain_text_editors), Zotero peut également être utilisé avec LaTeX, Scrivener et de nombreux autres logiciels de traitement de texte. 

### Bibliographies automatiques

![Citation insérée avec Zotero et bibliographie générée automatiquement dans un traitement de texte](./images/bibliography.png){class="QSGimg"}

En utilisant les [modules complémentaires pour traitements de texte](./word_processor_integration.md), il est possible de générer automatiquement une bibliographie à partir des documents cités et de modifier le style bibliographique de l'ensemble d'un document d'un simple clic.

### Bibliographies manuelles

![Créer manuellement une bibliographie à partie d'une sélection de documents](./images/manual_bib.png){class="QSGimg"}

Zotero peut aussi insérer [des citations et des bibliographies](./creating_bibliographies.md) dans n'importe quel champ de texte ou logiciel. Il vous suffit de glisser-déposer les documents, d'utiliser la "Copie rapide" pour copier les citations dans le presse-papier, ou de les exporter directement dans un fichier.

## Collaborer

### La synchronisation

![Icône d'état de la synchronisation dans le panneau de droite de la fenêtre de Zotero](./images/sync_petit_FR.png){class="QSGimg"}

Vous pouvez utiliser Zotero sur plusieurs ordinateurs grâce à la [synchronisation Zotero](./sync.md). Les notices et les notes sont synchronisées en utilisant les serveurs de Zotero (stockage illimité), tandis que les pièces jointes peuvent utiliser les serveurs de Zotero ou votre propre service WebDAV pour synchroniser les fichiers tels que les PDF, images, audio/vidéo.

### Les serveurs de Zotero

![La bibliothèque Zotero En ligne](./images/online_mylibrary_FR.png){class="QSGimg"}

Les [notices synchronisées](./sync.md) sur le serveur Zotero peuvent être consultées en ligne depuis votre compte [zotero.org](https://www.zotero.org/). Vous pouvez partager votre bibliothèque avec d'autres personnes ou créer un CV personnalisé en utilisant des notices sélectionnées.

Mettez des copies de vos recherches à la disposition des lecteurs, du public et d'autres chercheurs sur [zotero.org](https://www.zotero.org/) en utilisant [Mes publications](./my_publications.md).

### Les groupes

![Affichage des bibliothèques de groupe dans le panneau de gauche de la fenêtre de Zotero](./images/groups_FR.png){class="QSGimg"}

Les utilisateurs de Zotero peuvent créer des [groupes](./groups.md) collaboratifs ou d'intérêt. Les bibliothèques de groupe partagées permettent de gérer de manière collaborative les sources et le matériau de recherche, à la fois en ligne et via le client Zotero. [Zotero.org](https://www.zotero.org/) peut être le cœur de la recherche, de la communication et de l'organisation de votre groupe de travail. 
