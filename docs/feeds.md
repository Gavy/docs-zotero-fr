# Flux RSS

*Consulter cette page dans la documentation officielle de Zotero : [Feeds](https://www.zotero.org/support/feeds) - dernière mise à jour de la traduction : 2023-05-24*
{ data-search-exclude }

![Affichage des flux RSS enregistrés dans Zotero](./images/feeds_FR.png)

Les flux RSS sont un excellent moyen de découvrir de nouvelles recherches. Grâce aux flux, vous pouvez vous abonner aux mises à jour d'une revue, d'un site web, d'un éditeur, d'une institution, d'un groupe de recherche ou de toute autre source et trouver rapidement de nouveaux articles ou travaux. Si vous trouvez un article dans le flux d'une publication que vous souhaitez enregistrer et lire plus tard, vous pouvez l'ajouter à votre bibliothèque Zotero en un clic.
  
## S'abonner à des flux

![Capture d'écran du menu d'ajout de flux](./images/add_feed_FR.png)

Pour vous abonner à un flux, cliquez sur le bouton nouveau Flux au-dessus du volet de gauche dans la fenêtre Zotero. Vous pouvez ajouter des flux de deux façons. Tout d'abord, vous pouvez ajouter un flux en utilisant l'URL fournie sur le site web de la revue (ou d'une autre source). Pour les revues, ces flux sont généralement disponibles à partir de la page d'accueil de la revue. Cherchez l'icône RSS (![Icône RSS standard](./images/rss-icon.png)) ou cherchez le nom de la revue et "flux RSS" dans un moteur de recherche. De nombreux éditeurs placent l'icône RSS à côté des icônes de médias sociaux ou des liens d'alerte par courriel.

| Liens RSS sur ScienceDirect                     | Icône RSS pour Sage Journals            |
|:-----------------------------------------------:|:---------------------------------------:|
| ![Capture d'écran: liens RSS sur ScienceDirect](./images/rss_elsevier.png) | ![Capture d'écran: icône RSS sur Sage Journals](./images/rss_sage.png) |
|                                                 |                                         |

Une fois que vous avez trouvé l'URL du flux RSS de votre source, cliquez sur le menu "Nouveau flux", puis choisissez "A partir de l'URL...". La fenêtre des Paramètres du flux s'ouvrira. Collez l'URL du flux RSS dans le champ URL. Si l'URL est celle d'un flux valide, vous pourrez afficher et modifier son titre et cliquer sur "Enregistrer". Vous pouvez cliquer sur "Options avancées" pour ajuster la fréquence de mise à jour du flux et pour définir la durée pendant laquelle les documents lus et non lus sont conservés dans un flux avant d'être supprimés.

![Interface d'ajout de flux](./images/add_feed_url_FR.png)

Vous pouvez également importer un ensemble de flux à l'aide d'un fichier OPML (par exemple à partir d'une liste d'abonnements exportée de Feedly ou d'autres services de lecteurs RSS). Pour importer un fichier OPML, choisissez "A partir d'un fichier OPML..." dans le menu Nouveau flux.

## Lire des flux

![Fenêtre de lecture des flux](./images/feed_window_FR.png)

Les flux auxquels vous êtes abonné apparaîtront en bas du volet gauche de la fenêtre Zotero, sous Ma bibliothèque et vos Bibliothèques de groupe, et au-dessus du sélecteur de marqueurs. Cliquez sur un flux pour voir les articles actuellement disponibles dans ce dernier. Cliquez avec le bouton droit de la souris sur un flux pour l'actualiser manuellement, marquer tous les documents du flux comme lus, modifier les paramètres ou vous désabonner du flux.

Lors de la consultation d'un flux, les documents non lus sont affichés en gras, tandis que les documents lus sont affichés en texte normal. Lorsque vous sélectionnez un article dans le flux, vous pouvez le marquer comme lu ou non lu en cliquant sur "Marquer comme non lu/lu". Enregistrez l'article dans votre bibliothèque Zotero en cliquant sur "Ajouter à Ma bibliothèque" ou en appuyant sur Ctrl/Cmd-Maj-S. Vous pouvez enregistrer un article de flux dans une bibliothèque de groupe en cliquant sur la flèche déroulante à droite du bouton "Enregistrer dans ma bibliothèque" et en choisissant la bibliothèque correspondante.

*Notez que l'état lu/non lu des documents n'est pas synchronisé entre les ordinateurs lorsque vous utilisez la synchronisation Zotero.*
