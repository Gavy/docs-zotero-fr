# Comment puis-je ouvrir plusieurs instances de Zotero? Comment puis-je enregistrer dans et citer depuis une instance spéficique ?

*Consulter cette page dans la documentation officielle de Zotero : [How can I open multiple instances of Zotero and save to or cite from a specific instance?](https://www.zotero.org/support/kb/multiple_instances) - dernière mise à jour de la traduction : 2023-08-24*
{ data-search-exclude }

Vous pouvez lancer plusieurs instances de Zotero en même temps tant qu'elles pointent vers [des profils et des répertoires de données différents](./multiple_profiles.md). Sous Windows ou Linux, [l'option -no-remote](http://kb.mozillazine.org/Opening_a_new_instance_of_Firefox_with_another_profile) peut également être requise.

Des paramétrages additionnels peuvent être nécessaire pour l'intégration des navigateurs et traitements de texte, comme décrit ci-dessous.

Notez que l'utilisation de plusieurs instances de Zotero utilisera plus de mémoire, donc selon la quantité de mémoire de votre ordinateur utiliser plusieurs instances simultanément pourrait être plus lent que d'utiliser une seule instance avec davantage de données.

## Connecteur Zotero

Par défaut, lorsque vous exécutez plusieurs instances de Zotero (soit en utilisant plusieurs profils, soit en exécutant Zotero avec des comptes utilisateur distincts), le connecteur Zotero enregistre les documents dans la première instance ouverte. Pour faire pointer une installation de connecteur Zotero spécifique vers un profil Zotero spécifique, vous pouvez paramétrer [l'option cachée](https://www.zotero.org/support/preferences/hidden_preferences) `extensions.zotero.httpServer.port` dans Zotero et l'option `connector.url` dans le connecteur. Augmenter le numéro de port de 1 pour un couple (profil Zotero + installation de connecteur) est suffisant.

## Intégration pour les traitements de texte

### Word

#### Word pour Mac (Zotero 6) / Word pour Windows

Le module pour Word devrait se connecter automatiquement à la bonne instance Zotero sur un système multi-utilisateurs. Il n'est pas possible de faire pointer le module Word vers une instance spécifique à l'intérieur d'un même compte utilisateur.

#### Word pour Mac (Zotero 7)

Ouvrez le [dossier de démarrage Word](../word_processor_plugin_manual_installation.md#localiser-votre-dossier-de-démarrage-word) de l'utilisateur et créez un fichier `ZoteroPort.txt`​ contenant le numéro de port que vous avez configuré dans `​extensions.zotero.httpServer.port`​.

### Google Docs

Le module Google Docs dépend de la bonne configuration du connecteur Zotero à une instance spécifique, comme décrit ci-dessus.

### LibreOffice

Le module LibreOffice un port HTTP fixe pour se connecter à Zotero et ne peut actuellement pas être configuré, donc il se connectera à la première instance ouverte.
