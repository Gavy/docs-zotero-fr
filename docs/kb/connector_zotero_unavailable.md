# Connecteur Zotero : "Zotero est-il ouvert ?"

*Consulter cette page dans la documentation officielle de Zotero : [Zotero Connector: “Is Zotero Running?”](https://www.zotero.org/support/kb/connector_zotero_unavailable) - dernière mise à jour de la traduction : 2023-08-29*
{ data-search-exclude }

Lorsque vous cliquez sur le bouton Enregistrer dans Zotero dans votre navigateur ou que vous essayez d'utiliser Zotero avec Google Docs, vous pouvez recevoir le message suivant :

"Le Connecteur Zotero n'a pas pu communiquer avec l'application Zotero".

Le connecteur Zotero a besoin de se connecter à l’application de bureau Zotero pour pouvoir enregistrer des données ou insérer des citations dans Google Docs. (Il peut également enregistrer des pages directement sur zotero.org, mais l'enregistrement sur l'application Zotero offre la meilleure expérience).

Tout d'abord, assurez-vous que Zotero est installé et ouvert sur votre ordinateur. Si vous n'avez pas encore Zotero, vous pouvez l'installer à partir de la [page des téléchargements](https://www.zotero.org/download). Si vous utilisiez précédemment Zotero pour Firefox, notez que Zotero fonctionne désormais uniquement en tant qu'[application autonome](https://www.zotero.org/blog/a-unified-zotero-experience/).

Ensuite, redémarrez votre navigateur et réessayez.

Après le redémarrage, si Zotero est ouvert mais que le connecteur Zotero signale toujours que Zotero est indisponible, quelque chose sur votre ordinateur empêche le connecteur de communiquer avec Zotero. Vous pouvez déterminer si le problème se situe dans le navigateur ou dans l'ensemble du système en chargeant l'URL `http://127.0.0.1:23119/connector/ping` dans un ou plusieurs navigateurs alors que Zotero est ouvert. Si Zotero est en cours d'exécution, il devrait afficher "Zotero is running" ou "Zotero Connector Server is Available".

* Si vous pouvez charger cette URL mais que le connecteur Zotero indique toujours que Zotero est indisponible, essayez les étapes suivantes jusqu'à ce que le problème soit résolu :
    1. Désinstallez et réinstallez le connecteur Zotero.
    2. Désactivez temporairement toute extension de navigateur susceptible de bloquer les requêtes réseau, comme AdBlock, uBlock Origin, NoScript, EFF Privacy Badger ou Request Policy. Si le connecteur cesse d'indiquer que Zotero est hors ligne, réactivez chaque extension l'une après l'autre et, si le problème se reproduit, mettez sur la liste blanche `127.0.0.1` port `23119` dans les paramètres de l'extension.
    3. Désactivez temporairement toute autre extension installée.
    4. Essayez dans un autre navigateur. Si le problème se produit là aussi, il est probable qu’un logiciel de sécurité de votre ordinateur bloque les demandes d'extension sur plusieurs navigateurs.
    5. Si l'autre navigateur fonctionne, créez un nouveau profil dans votre navigateur d'origine. Si cela résout le problème, il y a un problème avec votre profil d'origine et vous devrez soit identifier le problème, soit transférer vos signets, votre historique et d'autres données vers le nouveau profil. Si le problème se produit dans un nouveau profil, il se peut que le logiciel de sécurité de votre ordinateur interfère avec les demandes d'extension uniquement dans ce navigateur particulier.

* Si vous ne parvenez pas à charger l'URL dans un navigateur mais que vous y parvenez dans un autre, essayez ces étapes dans le navigateur d'origine jusqu'à ce que l'URL fonctionne :
    1. Si votre ordinateur se connecte via un serveur proxy, assurez-vous que l'hôte `127.0.0.1` (un alias pour votre ordinateur lui-même) est exclu du proxy dans les paramètres du navigateur ou du proxy système.
    2. Désactivez temporairement toute extension de navigateur susceptible de bloquer les requêtes réseau, comme AdBlock, uBlock Origin, NoScript, EFF Privacy Badger ou Request Policy. Si l'URL commence à fonctionner, réactivez chaque extension l'une après l'autre et, si le problème se reproduit, mettez sur la liste blanche `127.0.0.1`  port `23119` dans les paramètres de l'extension.
    3. Désactivez temporairement toute autre extension installée.

* Si vous ne parvenez pas à charger l'URL dans aucun navigateur, essayez ces étapes jusqu'à ce que l'URL fonctionne :
    1. Si votre ordinateur se connecte via un serveur proxy, assurez-vous que l'hôte `127.0.0.1` (un alias pour votre ordinateur lui-même) est exclu du proxy.
    2. Redémarrez votre ordinateur.
    3. Désactivez temporairement tout logiciel de sécurité fonctionnant sur votre système. Si l'URL commence à fonctionner, réactivez chaque logiciel un par un et, si le problème se reproduit, mettez le port `127.0.0.1`  port `23119` sur la liste blanche dans les paramètres du logiciel.
    4. Redémarrez Zotero avec le journal de débogage activé via Aide → Journal de débogage → " Redémarrer avec le journal activé", puis allez dans Aide → Journal de débogage → Voir le journal. Copiez le journal dans un fichier texte et recherchez "serveur HTTP". Vous devriez voir l'un des trois messages suivants
        * `HTTP server listening on 127.0.0.1:23119` - Zotero écoute avec succès et quelque chose d'autre bloque la connexion.
        * `Not initializing HTTP server` - Zotero n'a pas pu écouter sur le port 23119, peut-être parce qu'un autre programme le faisait déjà. Ce message doit être précédé d'une erreur qui peut fournir plus d'informations.
        * `Browser is offline - not initializing HTTP server` - Zotero n'a pas du tout pu détecter une connexion réseau.

    5. Il peut être utile d'essayer dans un nouveau compte utilisateur sur votre système d’exploitation. (Vous n'avez pas besoin de configurer la synchronisation dans Zotero - une bibliothèque vide est suffisante pour tester). Si l'URL commence à fonctionner, vous devrez déterminer ce qui, dans votre compte d'origine, bloque la connexion, en fonction du message que vous voyez dans la sortie de débogage de Zotero. Si l'URL ne fonctionne pas non plus dans le nouveau compte, c'est qu'un logiciel système de votre ordinateur bloque la connexion, et vous devrez résoudre ce problème.

