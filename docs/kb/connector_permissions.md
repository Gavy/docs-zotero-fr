# Pourquoi mon navigateur indique-t-il que le connecteur Zotero doit avoir accès à mes données sur tous les sites Web ?

*Consulter cette page dans la documentation officielle de Zotero : [WWhy is my browser saying the Zotero Connector needs access to my data on all websites?](https://www.zotero.org/support/kb/connector_permissions) - dernière mise à jour de la traduction : 2022-11-15*
{ data-search-exclude }


Lorsque vous installez le connecteur Zotero dans votre navigateur, ce dernier vous indique les autorisations requises par l'extension :

* Chrome : "Lire et modifier toutes vos données sur tous les sites web".
* Firefox : "Accéder à vos données sur tous les sites web"
* Safari : "Peut lire les informations sensibles des pages Web, notamment les mots de passe, les numéros de téléphone et les cartes de crédit. Peut modifier l'apparence et le comportement des pages Web. Ceci s'applique à toutes les pages Web."

Il s'agit de l'autorisation standard dont une extension a besoin pour interagir avec le contenu des pages Web lorsque vous naviguez sur le Web. Le connecteur Zotero en a besoin pour détecter le contenu de la page et mettre à jour l'icône d'enregistrement avec le type de document détecté (par exemple, une icône de livre pour "Enregistrer sur Zotero (Amazon)", une icône PDF pour "Enregistrer sur Zotero (PDF)"), ainsi que pour fournir des fonctionnalités avancées telles que la redirection automatique vers un proxy et l'importation automatique RIS/BibTeX.

Aucune donnée concernant les pages que vous visitez n'est enregistrée sur votre ordinateur ou envoyée aux serveurs de Zotero, à moins que vous ne cliquiez sur le bouton d'enregistrement, auquel cas le connecteur enregistre les données et les fichiers soit dans l'application Zotero sur votre ordinateur (si elle est ouverte), soit dans votre bibliothèque en ligne sur zotero.org (si vous avez autorisé le connecteur à le faire et si vous vous êtes connecté).

Si l'enregistrement échoue après avoir cliqué sur le bouton d'enregistrement et que l'option "Report broken site translators to zotero.org" est activée dans les préférences du connecteur Zotero, le connecteur Zotero enverra un rapport d'erreur anonyme, comprenant l'URL, le navigateur et les informations sur la version aux serveurs de Zotero, afin que nous puissions résoudre plus rapidement les problèmes de compatibilité du site. Nous conservons ces informations pendant une semaine au maximum. Aucune autre information d'identification personnelle (par exemple, le nom d'utilisateur ou l'adresse IP) n'est stockée, et les rapports ne sont généralement consultés uniquement sous forme agrégée.

Pour plus d'informations sur les données collectées par Zotero, consultez notre [politique de confidentialité](../privacy.md).
