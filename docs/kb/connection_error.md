# "Erreur de connexion au serveur. Vérifiez votre connexion Internet."

*Consulter cette page dans la documentation officielle de Zotero : ["Error connecting to server. Check your Internet connection."](https://www.zotero.org/support/kb/connection_error) - dernière mise à jour de la traduction : 2023-12-07*
{ data-search-exclude }

Lorsque Zotero ne peut pas accéder au réseau, cela est généralement dû aux paramètres du proxy ou à un logiciel de sécurité sur votre ordinateur.

Par défaut, Zotero utilise tous les serveurs proxy saisis manuellement ou une URL de configuration automatique de proxy (*proxy auto-config*, *PAC*) dans les paramètres de proxy de votre système. Il n'utilise pas automatiquement le *Web Proxy Auto-Discovery* (*WPAD*), ou l'"*Auto Proxy Discovery*" sur macOS, même si cette option est activée dans les paramètres du système.

Si vous n'utilisez pas de proxy pour vous connecter à l'internet, vous devez désactiver tous les proxys dans les paramètres proxy de votre système. Si vous devez vous connecter via un proxy, vérifiez que les paramètres du système sont corrects. Notez que d'autres logiciels sur votre ordinateur peuvent ne pas utiliser les paramètres proxy du système, ce qui explique pourquoi d'autres programmes peuvent encore se connecter à Internet alors que Zotero ne le peut pas.

Si vous devez configurer les paramètres du proxy de Zotero différemment des paramètres du système, vous pouvez accéder à l'éditeur de configuration à partir du panneau Avancées des Préférences de Zotero, appliquer les [mêmes paramètres que dans Firefox](http://kb.mozillazine.org/Network.proxy.type) (sur lequel Zotero est basé), et redémarrer Zotero. Notez que le paramètre par défaut (network.proxy.type = 5, pour utiliser les paramètres du proxy du système) est recommandé.

Si vous utilisez un fichier *PAC*, soit automatiquement, soit avec network.proxy.type = 2, et que votre proxy nécessite une authentification HTTP, assurez-vous que la plupart ou tous les hôtes dans `extensions.zotero.proxyAuthenticationURLs` sont gérés par votre fichier *PAC*. Zotero teste un sous-ensemble aléatoire à chaque démarrage pour déclencher une demande d'authentification au proxy si nécessaire.

Pour utiliser *WPAD*, vous devez définir network.proxy.type à 4.

Si la modification des paramètres du proxy ne résout pas le problème, essayez de désactiver temporairement tout logiciel de sécurité/pare-feu sur votre système.

Certaines erreurs de connexion peuvent également être dues à des [problèmes de certificats](https://www.zotero.org/support/kb/ssl_certificate_error) sur votre réseau.

Voir aussi [Quelles connexions dois-je autoriser à travers un pare-feu pour que Zotero fonctionne correctement ?](https://www.zotero.org/support/kb/zotero_and_firewalls).