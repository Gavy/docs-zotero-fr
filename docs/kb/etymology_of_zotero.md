# Que signifie "Zotero" ?

*Consulter cette page dans la documentation officielle de Zotero : [What does “Zotero” mean?](https://www.zotero.org/support/kb/etymology_of_zotero) - dernière mise à jour de la traduction : 2023-05-02*
{ data-search-exclude }

Le nom "Zotero" est librement adapté du mot albanais (oui, albanais) zotëroj, qui signifie "acquérir, maîtriser", comme on le fait en apprenant. Pour plus d'informations, consultez le billet du linguiste Mark Dingemanse [The Etymology of Zotero](http://ideophone.org/zotero-etymology/).
