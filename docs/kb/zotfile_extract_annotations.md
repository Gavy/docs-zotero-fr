# Où le bouton "Extraire les annotations" est-il passé?

*Consulter cette page dans la documentation officielle de Zotero : [Where did the Extract Annotations button go?](https://www.zotero.org/support/kb/zotfile_extract_annotations) - dernière mise à jour de la traduction : 2023-02-23*
{ data-search-exclude }


Le bouton "Extraire les annotations" était une fonctionnalité du module complémentaire tiers ZotFile. Dans Zotero 6, cette fonctionnalité a été remplacée par les fonctionnalités avancées de PDF propres à Zotero.

Vous pouvez créer une note à partir de toutes les annotations d'un document en faisant un clic droit sur ce document dans la liste des documents et en sélectionnant "Ajouter une note depuis les annotations". Vous pouvez également ouvrir le PDF dans le nouveau lecteur PDF intégré et ajouter des annotations de manière sélective. Voir [Ajout d'annotations aux notes](../pdf_reader.md#ajout-dannotations-aux-notes) pour en savoir plus sur les différentes façons d'utiliser les annotations dans les notes dans Zotero 6.

Pour plus de détails sur cette nouvelle fonctionnalité, consultez la [présentation de Zotero 6 : "Zotero 6 : transformer votre flux de travail de recherche"](https://zotero.hypotheses.org/4145).

![L'option "Ajouter une note depuis les annotations" s'affiche dans le menu contextuel d'un document, lorsque vous ajoutez des annotations au PDF qui lui est attaché](../images/add-note-from-annotations_FR.png)

