# Comment changer la taille des polices du texte dans Zotero ?

*Consulter cette page dans la documentation officielle de Zotero : [How do I change the font size of text in Zotero?](https://www.zotero.org/support/kb/font_size) - dernière mise à jour de la traduction : 2023-03-22*
{ data-search-exclude }

Vous pouvez changer la taille des polices de l'interface utilisateur et des notes à partir du menu "Affichage".

Vous pouvez modifier la taille de la police de l'interface utilisateur et des notes à partir du menu "Affichage".