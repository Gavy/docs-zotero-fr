# Ma capture de page web est illisible parce que des publicités flash apparaissent au-dessus du texte de l'article. Comment puis-je obtenir une meilleure capture?

*Consulter cette page dans la documentation officielle de Zotero : [My snapshot is unreadable because flash ads appear on top of article text. How do I get a better snapshot?](https://www.zotero.org/support/kb/snapshot_is_unreadable) - dernière mise à jour de la traduction : 2023-04-06*
{ data-search-exclude }

Recherchez une version imprimable, puis enregistrez la capture de page web. Les versions imprimées des articles ne comportent généralement qu'une seule publicité et leurs captures sont plus faciles à lire.
