# Comment ajouter une lettre ou un mémo ?

*Consulter cette page dans la documentation officielle de Zotero : [How do I add a letter or memo?](https://www.zotero.org/support/kb/adding_a_letter_or_memo) - dernière mise à jour de la traduction : 2022-12-19*
{ data-search-exclude }

Cliquez sur le bouton vert "Nouveau document" et sélectionnez "Lettre". Utilisez "Auteur" pour l'expéditeur de la lettre. Pour ajouter un destinataire, cliquez sur le signe + sur la ligne de l'auteur dans la colonne de droite. Cela créera une ligne supplémentaire pour l'auteur. Si vous cliquez sur le triangle à gauche du nouveau champ "Auteur", vous pourrez changer "Auteur" en "Destinataire". Entrez le type de lettre (mémo, télégramme, etc.) dans le champ "Type". Pour la saisie d'informations archivistiques, voir [Comment ajouter une archive ou une autre source non publiée ?](./archival_or_other_unpublished_sources.md).

Si vous disposez d'un scan de la lettre (par exemple un PDF ou une image), vous pouvez [l'ajouter comme pièce jointe au document "Lettre"](../attaching_files.md).

