# J'ai des bibliographies dans des documents Microsoft Word, des PDF, et d'autres fichiers textes. Puis-je les importer dans ma bibliothèque Zotero ?

*Consulter cette page dans la documentation officielle de Zotero : [I have bibliographies in Microsoft Word documents, PDFs, and other text files. Can I import them into my Zotero library?](https://www.zotero.org/support/kb/importing_formatted_bibliographies) - dernière mise à jour de la traduction : 2023-03-24*
{ data-search-exclude }


## Citations insérées à l'aide d'un logiciel de gestion bibliographique

Zotero peut lire les citations existantes créées par les modules de traitements de texte de Zotero et Mendeley Desktop, permettant de continuer à utiliser ces citations dans le même document, même si les notices n'existent pas dans votre bibliothèque Zotero. Cliquez simplement sur le bouton "Add/Edit Citation", cherchez une citation existante, et sélectionnez-la depuis la section "Cité" des résultats de la recherche. (Cette méthode s'applique seulement à la fenêtre de dialogue par défaut, pas à la fenêtre "Vue classique".)

Si un document contient des citations Zotero ou Mendeley Desktop qui ne sont pas dans votre bibliothèque et que vous avez besoin de modifier leurs métadonnées ou de les inclure dans d'autres documents, vous aurez besoin de les extraire vers votre bibliothèque. Pour les documents Word au format .docx et LibreOffice au format .odt, vous pouvez utiliser [Reference Extractor](https://rintze.zelle.me/ref-extractor/). Notez que pour continuer à utiliser le même document il vous faudra remplacer toutes les occurrences de la citation originale par des citations liées à la nouvelle notice ajoutée à votre bibliothèque. Lors de cette opération, assurez-vous vous de bien sélectionner la citation dans la section "Ma bibliothèque" des résultats de recherche de la fenêtre de dialogue, plutôt que dans la section "Cité".

Si vous avez toujours les références dans un logiciel de gestion bibliographique, vous pouvez les importer dans Zotero.

* Zotero dispose [d'un outil intégré d'importation depuis Mendeley](./mendeley_import.md), qui peut importer toutes les données et reconstruire automatiquement les liens de citation dans des documents existants.
* Pour les autres logiciels, exportez vos données dans un format comme RIS ou BibTeX, puis [importez le fichier dans Zotero](../adding_items_to_zotero.md#importer-depuis-dautres-outils). Vous devrez remplacer toutes les citations existantes dans tout document pour lequel vous souhaitez que Zotero génère une bibliographie correcte.


## Citations insérées en utilisant la fonctionnalité de citation intégrée à Microsoft Word

Vous pouvez suivre les étapes suivantes pour formater la bibliographie en un fichier BibTeX, que Zotero peut importer.

1.  Téléchargez cette [feuille de style bibliographique Word](https://gist.githubusercontent.com/JaimeChavarriaga/40166befb14f2fe5dac390688d9eaf03/raw/faf4aa3f72e553095f81f1440c3dce744c2755a2/bibtex.xsl).
2.  Enregistrez la feuille de style dans le dossier des styles bibliographiques de Word :
    * *Word 2016 / 2019 / Office 365 pour Windows :* `C:\Utilisateurs\<NomUtilisateur>\AppData\Roaming\Microsoft\Bibliography\Style`
    * *Word 2010 pour Windows :* `C:\Program Files\Microsoft Office\<Office version>\Bibliography\Style` ou `C:\Program Files (x86)\Microsoft Office\<Office version>\Bibliography\Style`
    * *Mac :* Allez dans le dossier des Applications. Faites un clic-droit sur Microsoft Word et choisissez "Show Package Contents". Naviguer jusqu'à : `Content/Resources/Style`
3.  Dans Word, modifiez votre style bibliographique en sélectionnant "BibTeX export" et copiez la bibliographie dans le presse-papiers.
4.  Utilisez la fonctionnalité de Zotero [Importer depuis le presse-papiers](./import_from_clipboard.md).

Pour continuer à utiliser le même document, vous aurez besoin de remplacer toutes les citations originales afin que Zotero puisse générer une bibliographie correcte.

# Citations et bibliographies en texte simple

Si les références comportent un ISBN, un DOI, un PubMed ID, un arXiv ID ou un bibcode ADS, vous pouvez utiliser la fonctionnalité de Zotero [Ajouter un document par son identifiant](/adding_items_to_zotero/#ajouter-un-document-par-son-identifiant) pour importer rapidement ces documents dans votre bibliothèque Zotero.

Si vous avez de nombreuses références bibliographiques, vous pouvez utiliser [AnyStyle](http://anystyle.io), un analyseur bibliographique en ligne écrit par un développeur de Zotero. Exportez les citations analysées au format BibTeX ou CSL-JSON et importez-les dans Zotero. 

Sinon, votre meilleure option est de trouver les documents en ligne dans un dépôt ou une base bibliographique pris en charge par Zotero, ou, en dernier recours, de saisir manuellement les références.

Dans tous les cas, vous aurez besoin de remplacer toutes les citations de chaque document pour lequel vous souhaitez que Zotero génère une bibliographie correcte.
