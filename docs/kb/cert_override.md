# Outrepasser les erreurs de certificat dans Zotero

*Consulter cette page dans la documentation officielle de Zotero : [Overriding Security Certificate Errors in Zotero](https://www.zotero.org/support/kb/cert_override) - dernière mise à jour de la traduction : 2023-08-24*
{ data-search-exclude }

**Note** : Ces instructions doivent être utilisées uniquement avec un logiciel de sécurité qui intercepte/scanne les connexions HTTPS, un serveur WebDAV avec un certificat auto-signé, ou un réseau institutionnel qui surveille le trafic chiffré à l'aide d'une autorité de certification (CA) racine personnalisée. Vous ne devez jamais ignorer les erreurs de certificat [si vous n'en connaissez pas les conséquences](https://www.zotero.org/support/kb/ssl_certificate_error). En cas de doute, veuillez contacter votre administrateur réseau ou votre fournisseur d'accès à Internet.

## Certificat auto-signé

Zotero ne fournit pas actuellement de moyen graphique pour mettre sur liste blanche les certificats auto-signés, vous devrez donc copier les fichiers à partir d'une installation Firefox fonctionnelle.

Si vous utilisez un serveur WebDAV avec un certificat auto-signé, vous pouvez ouvrir l'URL WebDAV dans Firefox, accepter le certificat, puis copier le fichier `cert_override.txt` depuis le [répertoire de profil de Firefox](http://support.mozilla.com/kb/Profiles) vers le [répertoire de profil de Zotero](./profile_directory.md).

### Zotero 6

Zotero 6 attend un fichier `cert_override.txt` créé par Firefox 60 ESR, avec une ligne de cette forme :

`192.168.xxx.xxx:1234    OID.2.16…    1D:E4:07:…    U    AAAA…`

Si vous créez un fichier override avec une version plus récente de Firefox, votre fichier `cert_override.txt` peut contenir une ligne avec un deux-points après le numéro de port (1234 dans cet exemple) et il peut manquer une ou plusieurs lettres avant "AAAA" ("U" dans l’exemple ci-dessus) :

`192.168.xxx.xxx:1234:    OID.2.16…    1D:E4:07:…    AAAA…`

Pour utiliser un tel fichier dans Zotero 6, il suffit de supprimer les deux points après le numéro de port et d'ajouter un "U" (pour untrusted certificate) avant "AAAA". Pour autoriser une incohérence de nom de domaine, ajouter "M".

### Zotero 7 (beta)

Zotero 7 peut lire un fichier cert_override.txt depuis Firefox 102 au minimum.

## Autorité de certification personnalisée

Si vous ou votre organisation utilisez une autorité de certification personnalisée, ce qui peut être le cas lorsque vous utilisez un logiciel de sécurité ou que vous vous connectez via un serveur proxy, Zotero peut avoir besoin d'être configuré pour accepter l'autorité de certification personnalisée :

* Windows : Zotero pour Windows utilisera automatiquement le magasin de certificats des autorités de certifications racines du système, ce qui, dans la plupart des cas, devrait lui permettre de fonctionner automatiquement comme les autres navigateurs du système.

* Mac/Linux : 
    * Zotero 6 est basé sur Firefox et utilise le même mécanisme de certificat. Vous ou votre service informatique devrez donc configurer Firefox pour l'autorité de certification personnalisée dans un nouveau profil Firefox, puis copier les fichiers `cert9.db`, `key4.db` et `pkcs11.txt` du [répertoire du profil Firefox](http://support.mozilla.com/kb/Profiles) vers le [répertoire du profil Zotero](./profile_directory.md).
        * Firefox 63 et les versions ultérieures utiliseront automatiquement le magasin de certificats racines du système sur macOS. Si Firefox utilise le magasin racine du système, votre service informatique n'a peut-être pas ajouté son certificat personnalisé à la base de données de certificats dans le répertoire de profil de Firefox, et la copie des fichiers susmentionnés dans le répertoire de profil de Zotero ne fonctionnera peut-être pas. Votre service informatique devra [désactiver security.enterprise_roots.enabled](https://support.mozilla.org/kb/setting-certificate-authorities-firefox) dans `about:config` et ajouter le certificat racine personnalisé à Firefox afin qu'il puisse se connecter correctement via la connexion interceptée de l'institution. Vous pouvez ensuite copier les fichiers ci-dessus dans le répertoire de profil de Zotero et réinitialiser le paramètre `security.enterprise_roots.enabled` dans Firefox.
        * Pour ajouter vous-même le certificat CA à la base de données des certificats, vous pouvez essayer d'utiliser [nss certutil](https://developer.mozilla.org/en-US/docs/Mozilla/Projects/NSS/tools/NSS_Tools_certutil) :
        `certutil -A -d $ZOTERO_PROFILE_PATH -n $CA_NICKNAME -t C -i $CA_CERT_FILE`
    * Zotero 7 prendra en charge l’utilisation automatiques des certificats racines du système. Zotero 7 est actuellement en [beta](https://forums.zotero.org/discussion/105094/announcing-the-zotero-7-beta), aussi vous pouvez préférer utiliser Zotero 7 au lieu d’essayer d’ajouter le certificat dans Zotero 6.