# Pourquoi n'y a-t-il pas de bouton d'enregistrement dans la barre d'outils de mon navigateur ?

*Consulter cette page dans la documentation officielle de Zotero : [Why is there no save button in my browser toolbar?](https://www.zotero.org/support/kb/no_toolbar_button) - dernière mise à jour de la traduction : 2023-08-24*
{ data-search-exclude }

Le connecteur Zotero place une icône d’enregistrement - représentant le contenu qu'il a reconnu sur la page que vous êtes en train de consulter (livre, article de journal, page Web) - à droite de la barre d'adresse dans la barre d'outils de votre navigateur :

![Icône d’enregistrement du navigateur ayant détecté un livre](../images/save_button_book.png)

Si vous ne la voyez pas, vérifiez d'abord que vous avez installé le connecteur Zotero pour votre navigateur à partir de la [page de téléchargement](https://www.zotero.org/download/) et qu'il apparaît comme activé dans le volet "Extensions" du navigateur. Si vous avez des difficultés à l'installer, vérifiez que vous utilisez bien une [version compatible du navigateur](https://www.zotero.org/support/system_requirements).

Si vous ne le trouvez toujours pas, essayez les étapes ci-dessous correspondant à votre navigateur.

## Chrome

Si le bouton n'apparaît pas à droite de la barre d'adresse, il peut également apparaître dans le panneau des extensions, accessible via une icône en forme de pièce de puzzle. Si vous trouvez le bouton dans ce panneau, cliquez dessus pour l'épingler afin qu'il reste visible.

Si l'extension est activée dans le panneau Extensions mais que le bouton n'apparaît nulle part dans les barres d'outils, essayez de désinstaller et de réinstaller l'extension. S'il n'apparaît toujours pas, il se peut que les paramètres de Chrome soient corrompus. Vous pouvez essayer de [réinitialiser Chrome](https://support.google.com/chrome/answer/3296214).

## Edge

Si le bouton n'apparaît pas à droite de la barre d'adresse, il peut également apparaître dans le panneau des extensions, accessible via une icône en forme de pièce de puzzle. Si vous trouvez le bouton dans ce panneau, cliquez sur l'icône "Afficher dans la barre d'outils" pour l'épingler afin qu'il reste visible.

Si l'extension est activée dans le panneau Extensions mais que le bouton n'apparaît nulle part dans les barres d'outils, essayez de désinstaller et de réinstaller l'extension. Si elle n'apparaît toujours pas, vos paramètres Edge sont peut-être corrompus et vous pouvez essayer de les réinitialiser (Paramètres → Réinitialiser les paramètres).

## Firefox

Si le bouton n'apparaît pas à droite de la barre d'adresse, vous pouvez le trouver dans le menu Extensions (ouvert par un bouton en forme de pièce de puzzle à droite des autres boutons de la barre d'outils) ou dans le menu de dépassement (ouvert par un bouton "»" à droite des autres boutons de la barre d'outils). Si le bouton apparaît dans le menu Extensions, cliquez sur l'icône de l'engrenage et sélectionnez "Épingler à la barre d'outils" pour le conserver dans la barre d'outils.


Si vous ne le voyez toujours pas, il se peut que votre profil Firefox soit corrompu. Essayez de créer un nouveau profil Firefox et d'y installer le connecteur Zotero.


Si vous avez essayé toutes ces étapes et que vous rencontrez toujours des difficultés, il se peut que votre profil Firefox soit corrompu. Essayez de créer un [nouveau profil Firefox](http://support.mozilla.com/kb/Profiles) et d'installer le connecteur Zotero dans ce profil.

## Safari

Voir [Compatibilité Safari](./safari_compatibility.md).