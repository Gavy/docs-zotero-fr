# Pourquoi Zotero ne dispose-t-il pas d'une fonctionnalité de "dossier de veille" ?

*Consulter cette page dans la documentation officielle de Zotero : [Why doesn't Zotero have a “watch folder” feature?](https://www.zotero.org/support/kb/watch_folder) - dernière mise à jour de la traduction : 2023-12-13*
{ data-search-exclude }

Les personnes venant d'autres outils basés sur un "dossier de veille" recherchent parfois une fonctionnalité similaire dans Zotero et sont surprises de constater qu'elle n'existe pas.

Zotero est conçu autour d'un flux de travail différent de ces autres outils, et cela reflète un peu une différence philosophique. Nous pensons que vous ne devriez pas avoir à télécharger manuellement des fichiers et à vous soucier des chemins de dossiers. Zotero est profondément intégré au navigateur dans lequel vous effectuez la plupart de vos recherches, et il possède une capacité inégalée à enregistrer des métadonnées de haute qualité à partir du Web. 

Pour enregistrer dans Zotero, il vous suffit de [cliquer sur le bouton d'enregistrement](../adding_items_to_zotero.md#via-votre-navigateur-web) lorsque vous êtes sur la page d'un article : Zotero enregistre les détails bibliographiques du document et télécharge automatiquement le PDF associé s'il est disponible.

Si seul un PDF est disponible, vous pouvez toujours l’enregistrer directement avec le bouton d’enregistrement, et Zotero tente alors de [récupérer les métadonnées](../retrieve_pdf_metadata.md). Assurez-vous que votre navigateur est paramétré pour prévisualiser directement les PDFs à une URL `http://` ou `https://`, plutôt que de télécharger les PDF et de les afficher à une URL `file://` à laquelle le connecteur Zotero ne peut pas accéder.

Si vous avez déjà un fichier local sur le disque (par exemple, reçu par courriel), vous pouvez le faire glisser dans Zotero, mais ce n'est pas le principal moyen d'ajouter des documents.

Une fois que vous avez enregistré un document, Zotero gère vos fichiers pour vous, notamment en les renommant en fonction de l'auteur, de l'année et du titre du document parent et, si vous utilisez la synchronisation des fichiers, en veillant à ce qu'ils restent liés d'un appareil à l'autre. Il existe des extensions qui peuvent vous aider à [organiser vos fichiers différemment](../attaching_files.md#fichiers-joints-et-fichiers-liés) si vous préférez, mais la gestion des fichiers se fait toujours automatiquement pour vous en fonction de vos paramètres, et il ne s'agit pas de quelque chose que vous devez gérer manuellement.

En fin de compte, Zotero est conçu pour faire gagner du temps, et nous nous efforçons de ne pas mettre en œuvre des fonctionnalités qui, selon nous, encouragent des flux de travail plus fastidieux. Puisque Zotero est open source, la plupart des actions peuvent être réalisées avec des paramètres ou des extensions. Toutefois notre objectif est de créer une expérience par défaut qui fonctionne pour la plupart des gens : enregistrement des données et des fichiers en un clic, gestion et synchronisation automatiques des fichiers, organisation et recherche avancées dans l'application. Essayez-le !

Vous n'êtes toujours pas convaincu ? Lisez la section [Ajouter des documents à Zotero](../adding_items_to_zotero.md) pour mieux comprendre les différentes façons d'introduire des données et des fichiers dans Zotero, et publiez vos questions ou commentaires dans les [forums de Zotero](https://forums.zotero.org/). Nous sommes toujours heureux de discuter de nos décisions de conception et nous pouvons souvent recommander une meilleure façon de faire quelque chose si vous avez des difficultés. 
