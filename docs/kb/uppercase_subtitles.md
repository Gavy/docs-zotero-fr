# Pourquoi la première lettre d'un sous-titre n'est-elle pas en majuscule dans les références bibliographiques ?

*Consulter cette page dans la documentation officielle de Zotero : [Why isn't the first letter of a subtitle in uppercase in bibliographies?(https://www.zotero.org/support/kb/uppercase_subtitles) - dernière mise à jour de la traduction : 2023-02-01*
{ data-search-exclude }


Certains styles qui exigent que toutes les initiales soient en majuscules, comme l'APA, exigent également que la première lettre du sous-titre qui suit les deux points soit également en majuscule ("Age and environmental sustainability : A meta-analysis").

Zotero met automatiquement une majuscule au sous-titre pour le style APA et les autres styles basés sur celui-là. Si vous rencontrez un style pour lequel le sous-titre devrait commencer par une majuscule et ne l'est pas actuellement, veuillez le [signaler sur les forums de Zotero](https://www.zotero.org/forum).
