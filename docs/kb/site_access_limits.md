# Google Scholar (ou un autre site) m'a bloqué après avoir utilisé Zotero pour enregistrer des documents. Que s'est-il passé ?

*Consulter cette page dans la documentation officielle de Zotero : [Google Scholar (or some other site) locked me out after using Zotero to save items. What happened?](https://www.zotero.org/support/kb/site_access_limits) - dernière mise à jour de la traduction : 2023-02-28*
{ data-search-exclude }

Lorsqu'ils enregistrent plusieurs documents à la fois, des utilisateurs de Zotero peuvent parfois repousser les limites d'accès (définies pour un individu et dans une période donnée) de Google Scholar ou de toute autre base de données compatible avec Zotero (par exemple, ProQuest). Lorsque cela se produit, l'enregistrement dans Zotero peut échouer, et lorsque vous naviguez sur le site, vous pouvez commencer à voir des CAPTCHA ou des messages concernant des requêtes automatisées.

Si vous devez importer plusieurs documents à la fois, il est préférable d'enregistrer d'abord les documents dans la fonction ["Ma bibliothèque"](https://scholar.google.com/intl/en/scholar/help.html#library) de Google Scholar, d'exporter les documents en masse sous forme de fichier BibTeX à partir de la fonction "Ma bibliothèque", puis d'importer le fichier BibTeX dans Zotero. D'autres sites peuvent avoir des fonctions similaires permettant d'exporter un lot de documents. Vous pouvez aussi simplement cliquer sur la page du document et l'enregistrer à partir de là plutôt que d'enregistrer à partir de la page des résultats de recherche.

Il peut arriver que le nombre de requêtes envoyées par le module complémentaire [Google Scholar Citations for Zotero](https://github.com/beloglazov/zotero-scholar-citations) suffise à ce que Google Scholar verrouille Zotero.

## Comment puis-je rétablir mon accès au site ?

Il arrive parfois qu'un site demande une vérification CAPTCHA. Dans ce cas, effectuez le test de vérification pour rétablir l'accès. Sinon, en général, le site rétablira votre accès en quelques heures. Dans l'intervalle, vous pourrez peut-être accéder au site à l'aide d'un autre navigateur Web.

Si vous pouvez toujours accéder au site avec votre navigateur mais que vous ne pouvez pas enregistrer dans Zotero, vous pouvez, comme solution temporaire, enregistrer dans un fichier au format BibTeX ou RIS, comme expliqué ci-dessus, et importer le fichier dans Zotero.
