# Zotero et la Text Encoding Initiative (TEI)

*Consulter cette page dans la documentation officielle de Zotero : [Zotero and the Text Encoding Initiative (TEI)](https://www.zotero.org/support/kb/tei) - dernière mise à jour de la traduction : 2023-05-02*
{ data-search-exclude }

La [Text Encoding Initiative](http://www.tei-c.org/) est un consortium qui élabore une norme internationale pour le balisage XML des textes. Cette norme bénéficie d'un large soutien de la part des bibliothèques, des musées, des éditeurs et des chercheurs. On trouve de plus en plus de documents en ligne conformes à la TEI.

La TEI fournit de riches métadonnées bibliographiques dans l'élément [`<teiHeader>`](http://www.tei-c.org/release/doc/tei-p5-doc/en/html/ref-teiHeader.html) et ses fils, dont certains sont modelés sur la [Description bibliographique internationale normalisée (ISBD)](https://fr.wikipedia.org/wiki/International_Standard_Bibliographic_Description) (voir la [note de la TEI à l'intention des catalogueurs de bibliothèques](http://www.tei-c.org/release/doc/tei-p5-doc/en/html/HD.html#HD8)). Pour coder les citations bibliographiques qui peuvent apparaître dans un texte, l'élément [`<biblStruct>`](http://www.tei-c.org/release/doc/tei-p5-doc/en/html/ref-biblStruct.html) est couramment utilisé pour une citation bibliographique structurée, dans laquelle seuls les sous-éléments bibliographiques apparaissent, dans un ordre spécifié.

La TEI et Zotero sont en théorie parfaitement compatibles, Zotero étant capable de détecter et d'importer un texte conforme à la TEI et, en retour, d'exporter vers un élément `<biblStruct>` bien formé. À l'heure actuelle, cette interaction est encore en cours de développement.

* Les enregistrements Zotero peuvent être exportés vers un XML conforme à la TEI.
* Zotero ne peut pas actuellement importer du XML conforme à la TEI. Un convertisseur d'importation pourrait être écrit, ou une feuille de style pourrait être créée pour convertir les balises TEI en RDF prêt pour Zotero.

Il existe un [groupe Zotero sur la TEI](http://www.zotero.org/groups/tei), au cas où vous voudriez vous familiariser avec ce qui a été publié sur la TEI. Les utilisateurs des communautés Zotero et TEI sont encouragés à ajouter de nouveaux outils à cette page au fur et à mesure qu'ils établissent des liens entre eux.
