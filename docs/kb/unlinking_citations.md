# Comment puis-je délier toutes les citations Zotero d'un document ?

*Consulter cette page dans la documentation officielle de Zotero : [How do I unlink all Zotero citations in a document?](https://www.zotero.org/support/unlinking_citations) - dernière mise à jour de la traduction : 2023-05-30*
{ data-search-exclude }

Avant de soumettre un document que vous avez créé avec Zotero, vous devez toujours délier les citations Zotero afin de ne laisser que le texte plat dans le document. Cela permet d'éviter les surlignages gênants et de prévenir des problèmes liés aux pipelines de publication.

**Déliez toujours les citations dans une copie du document.** Conservez la version originale du document avec les citations Zotero actives afin de pouvoir, le cas échéant, apporter des modifications en réponse à des commentaires (ou même, par exemple, remettre en forme le document selon un autre style bibliographique).

## Délier les citations avec Zotero

Pour délier les citations avec Zotero, il suffit d'utiliser le bouton "Unlink Citations" du module de traitement de texte dans votre copie du document.

## Délier les citations manuellement

Si, pour une raison quelconque, vous n'êtes pas en mesure d'utiliser Zotero pour délier les citations, vous pouvez le faire directement dans Microsoft Word. Dans une copie du document, sélectionnez tout le texte (Ctrl+A/Cmd+A) et appuyez sur Ctrl+Maj+F9/Cmd+Maj+Fn+F9. (Sur un Mac, vous pouvez également appuyer sur Cmd+6.)

Si vous ne parvenez pas à délier les citations avec Zotero dans un document Google Docs, vous pouvez télécharger ce dernier au format .docx, l'ouvrir dans Word et procéder de la même manière.

Notez que cette méthode aplatit tous les champs du document, et pas seulement les citations Zotero.
