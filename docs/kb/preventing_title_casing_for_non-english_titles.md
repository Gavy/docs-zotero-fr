# Comment éviter que chaque mot des titres non anglais ne commencent par une lettre capitale (casse du titre) dans les bibliographies?

*Consulter cette page dans la documentation officielle de Zotero : [How do I prevent title casing of non-English titles in bibliographies?](https://www.zotero.org/support/kb/preventing_title_casing_for_non-english_titles) - dernière mise à jour de la traduction : 2022-10-04*
{ data-search-exclude }


Certains styles bibliographiques, comme les styles Chicago Manual of Style, convertissent les titres en casse du titre, c'est-à-dire que chaque mot significatif du titre voit sa première lettre mise en capitale. Cependant cet usage de la casse est spécifique à l'anglais. Pour éviter que des titres non-anglais apparaissent ainsi, il suffit de préciser la langue du document correspondant dans votre bibliothèque Zotero, dans le champ "Langue".

Utilisez les codes de langue à deux lettres, par exemple "de" pour l'allemand, "fr" for le français, ou "ja" pour le japonais. Les codes à quatre lettres peuvent aussi être utilisés par exemple "de-DE", "ja-JP", etc. ; voyez <https://github.com/citation-style-language/locales/wiki> pour une liste des codes de localisation. Les documents en anglais peuvent être enregistrés comme tels en utilisant "en", "en-GB" (anglais britannique) ou "en-US" (anglais américain).

Les titres doivent d'une manière générale être stockés en casse de la phrase ; Zotero peut convertir automatiquement les titres en casse du titre, mais il ne peut pas effectuer une conversion fiable (en traitant correctement les abréviations et les noms propres) vers la casse de la phrase. Voyez [la page de la base de connaissance concernant l'usage de la casse](./sentence_casing.md) pour plus d'informations.
