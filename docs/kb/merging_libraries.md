# J'ai deux bibliothèques Zotero. Comment puis-je les combiner?

*Consulter cette page dans la documentation officielle de Zotero : [I have two Zotero libraries. How can I combine them?](https://www.zotero.org/support/kb/merging_libraries) - dernière mise à jour de la traduction : 2022-12-02*
{ data-search-exclude }

Vous allez exporter l'une des bibliothèques au format Zotero RDF, puis importer ce fichier .rdf dans la seconde bibliothèque.

Pour ce faire, cliquez sur "Fichier" → "Exporter la bibliothèque...". Le format d'exportation par défaut doit être "Zotero RDF", et vous devez le laisser sélectionné. Vous pouvez cocher les cases pour inclure les fichiers et les notes si vous souhaitez les transférer dans l'autre bibliothèque. Cliquez sur OK, choisissez un nom pour le fichier .rdf et enregistrez-le sur votre ordinateur. Vous pouvez ensuite envoyer ce dossier par courrier électronique à un ami ou à un collègue, ou encore à vous-même. Dans la seconde bibliothèque Zotero, cliquez à nouveau sur le menu "Fichier" et sélectionnez "Importer...". Choisissez le fichier .rdf que vous venez de créer et cliquez sur OK. Cette opération importe les documents, en combinant les deux bibliothèques. 
