# Dépannage des erreurs dans les documents de traitement de texte

*Consulter cette page dans la documentation officielle de Zotero : [Troubleshooting Errors in Word Processor Documents](https://www.zotero.org/support/kb/debugging_broken_documents) - dernière mise à jour de la traduction : 2023-10-19*
{ data-search-exclude }

**[17 octobre 2023] Un problème au niveau de Google interrompt actuellement l'intégration Zotero pour Google Docs pour de nombreuses personnes. Suivez [ce fil de discussion](https://forums.zotero.org/discussion/108606/google-docs-issues) pour obtenir les mises à jour à ce sujet.**

Suivez les étapes ci-dessous pour [Microsoft Word](#microsoft-word), [Google Docs](#google-docs) ou [LibreOffice](#libre-office).

## Microsoft Word

Si vous obtenez une erreur en essayant d'utiliser Zotero dans **un nouveau document, vide,** consultez la page [Dépannage du module pour logiciels de traitement de texte (en anglais)](https://www.zotero.org/support/word_processor_plugin_troubleshooting).

Si vous pouvez insérer des citations dans un nouveau document vide mais que vous obtenez une erreur lors de l'utilisation de Zotero **dans un document existant**, procédez comme suit.

1. Redémarrez à la fois Zotero et Word.
2. Assurez-vous que vous utilisez les dernières versions de Zotero et de Word.
3. Pendant le dépannage, désactivez la fonction de suivi des modifications dans Word, car elle peut avoir des effets compliqués lorsque vous travaillez avec Zotero. Si la fonction de suivi des modifications est activée lorsque vous insérez ou modifiez une citation Zotero, elle peut marquer plusieurs ou toutes les citations Zotero de votre document comme modifiées ou provoquer [l'affichage de codes de champ](./word_field_codes.md). En de rares occasions, le suivi des modifications peut amener Zotero à penser qu'une citation est corrompue. Si vous aviez activé le suivi des modifications auparavant, essayez d'accepter toutes les modifications pour voir si cela résout le problème.
4. Vérifiez les citations dans les légendes d'images. Zotero ne vous permet pas de les insérer, mais si vous avez copié une citation dans une légende, c'est très probablement la source du problème. Supprimez-la.
5. Essayez de copier et de coller le contenu du document dans un nouveau document pour voir si le problème disparaît. Vous devrez peut-être cliquer sur le bouton "Document Preferences" avant que vos anciennes citations ne soient reconnues.
6. Faites une copie de votre document - en dupliquant le fichier lui-même, et non en copiant et collant le contenu - à utiliser pour le débogage.
7. Si vous utilisez OneDrive sous Windows, enregistrez la copie du document sur votre disque dur local, ou essayez de renommer le fichier pour supprimer les espaces dans le nom de fichier. OneDrive est connu pour interférer avec le module Zotero pour certaines personnes. 
8. Ouvrez le fichier copié et vérifiez si vous obtenez l'erreur après avoir changé de style bibliographique.
9. Si le document comporte une bibliographie, supprimez-la et vérifiez si vous obtenez toujours l'erreur.
10. Pendant le débogage, si vous utilisez le mode Champs dans Word, il peut être utile d'afficher les codes de champ plutôt que le texte mis en forme. Pour ce faire, appuyez sur Alt/Option+F9 (ou Alt/Option+Fn+F9) dans Word.
11. **Isolez les citations qui posent problème**. Dans la copie de votre document, supprimez la moitié du contenu d'un seul coup et voyez si l'erreur se produit toujours. Si ce n'est pas le cas, utilisez la fonction Annuler pour restaurer la section supprimée, puis essayez de supprimer l'autre moitié. Répétez le processus de réduction de moitié sur la section qui échoue, ou choisissez-en une au hasard si les deux le font. Continuez ainsi jusqu'à ce que vous trouviez la plus petite section possible, idéalement avec une seule citation, qui doit être présente pour que le problème se produise. Supprimez les citations isolées du document d'origine et le problème devrait disparaître (à moins qu'il n'y ait plusieurs citations brisées, auquel cas vous devrez répéter le processus). À moins que l'erreur ne se produise encore si vous effacez complètement le contenu du document, **cette dernière étape permettra par définition d'identifier le problème.**

Si vous êtes face à un document cassé, veuillez créer un nouveau fil de discussion sur le [forum Zotero](https://www.zotero.org/forum) afin que nous puissions tenter de résoudre le problème. N'oubliez pas d'inclure [un identifiant de rapport](../reporting_problems.md#1-fournissez-un-report-id-identifiant-de-rapport) de Zotero, la version de votre système d'exploitation et de Word, ainsi que les étapes que vous avez tentées pour corriger l'erreur. Vous devez également envoyer l'extrait de document de l'étape 11 et un lien vers votre fil de discussion sur le forum à [support@zotero.org](mailto:support@zotero.org) afin que nous puissions essayer de reproduire le problème.

## Google Docs

Si vous obtenez une erreur en essayant d'utiliser Zotero dans **un nouveau document, vide,** consultez la [section "Résolution des problèmes" de la page "Utiliser Zotero dans Google Docs"](../google_docs.md/#résolution-des-problèmes).

Si vous pouvez insérer des citations dans un nouveau document vide mais que vous obtenez une erreur lors de l'utilisation de Zotero **dans un document existant**, procédez comme suit.

1. Redémarrez à la fois Zotero et votre navigateur.
2. Assurez-vous que vous utilisez les dernières versions de Zotero et du connecteur Zotero.
3. Désactivez toutes les autres extensions de votre navigateur et rechargez Google Docs.
4. Essayer d'utiliser Fichier → "Créer une copie" pour voir si le problème disparaît dans un nouveau document. Vous devrez peut-être cliquer sur le bouton "Document Preferences" avant que vos anciennes citations ne soient reconnues.
5. Dans la copie du document, vérifiez si vous obtenez l'erreur après avoir changé de style bibliographique.
6. Si le document comporte une bibliographie, supprimez-la et vérifiez si vous obtenez toujours l'erreur.
7. **Isolez les citations qui posent problème**. Dans la copie de votre document, supprimez la moitié du contenu d'un seul coup et voyez si l'erreur se produit toujours. Si ce n'est pas le cas, utilisez la fonction Annuler pour restaurer la section supprimée, puis essayez de supprimer l'autre moitié. Répétez le processus de réduction de moitié sur la section qui échoue, ou choisissez-en une au hasard si les deux le font. Continuez ainsi jusqu'à ce que vous trouviez la plus petite section possible, idéalement avec une seule citation, qui doit être présente pour que le problème se produise. Supprimez les citations isolées du document d'origine et le problème devrait disparaître (à moins qu'il n'y ait plusieurs citations brisées, auquel cas vous devrez répéter le processus). À moins que l'erreur ne se produise encore si vous effacez complètement le contenu du document, **cette dernière étape permettra par définition d'identifier le problème.**

Si vous êtes face à un document cassé, veuillez créer un nouveau fil de discussion sur le [forum Zotero](https://www.zotero.org/forum) afin que nous puissions tenter de résoudre le problème. N'oubliez pas d'inclure [un Debug ID](../debug_output.md) du connecteur Zotero pour reproduire le problème, ainsi que les étapes que vous avez tentées pour corriger l'erreur. Vous devez également générer un lien de partage pour l'extrait de document de l'étape 7 et l'envoyer à [support@zotero.org](mailto:support@zotero.org) avec un lien vers votre fil de discussion sur le forum, afin que nous puissions essayer de reproduire le problème.

## Libre Office


Si vous obtenez une erreur en essayant d'utiliser Zotero dans **un nouveau document, vide,** consultez la page [Dépannage du module pour logiciels de traitement de texte (en anglais)](https://www.zotero.org/support/word_processor_plugin_troubleshooting).

Si vous pouvez insérer des citations dans un nouveau document vide mais que vous obtenez une erreur lors de l'utilisation de Zotero **dans un document existant**, procédez comme suit.

1. Redémarrez à la fois Zotero et LibreOffice.
2. Assurez-vous que vous utilisez les dernières versions de Zotero et de LibreOffice.
3. Pendant le dépannage, désactivez la fonction de suivi des modifications dans LibreOffice, car elle peut avoir des effets compliqués lorsque vous travaillez avec Zotero. Si la fonction de suivi des modifications est activée lorsque vous insérez ou modifiez une citation Zotero, elle peut marquer plusieurs ou toutes les citations Zotero de votre document comme modifiées ou provoquer [l'affichage de codes de champ](./word_field_codes.md). En de rares occasions, le suivi des modifications peut amener Zotero à penser qu'une citation est corrompue. Si vous aviez activé le suivi des modifications auparavant, essayez d'accepter toutes les modifications pour voir si cela résout le problème.
4. Vérifiez les citations dans les légendes d'images. Zotero ne vous permet pas de les insérer, mais si vous avez copié une citation dans une légende, c'est très probablement la source du problème. Supprimez-la.
5. Essayez de copier et de coller le contenu du document dans un nouveau document pour voir si le problème disparaît. Vous devrez peut-être cliquer sur le bouton "Set Document Preferences" avant que vos anciennes citations ne soient reconnues.
6. Faites une copie de votre document - en dupliquant le fichier lui-même, et non en copiant et collant le contenu - à utiliser pour le débogage.
7. Ouvrez le fichier copié et vérifiez si vous obtenez l'erreur après avoir changé de style bibliographique.
8. Si le document comporte une bibliographie, supprimez-la et vérifiez si vous obtenez toujours l'erreur.
9. Pendant le débogage, si vous utilisez le mode Marques de référence dans LibreOffice, il peut être utile d'afficher les codes de champ plutôt que le texte mis en forme, en appuyant sur Ctrl+F9 dans LibreOffice.
10. **Isolez les citations qui posent problème**. Dans la copie de votre document, supprimez la moitié du contenu d'un seul coup et voyez si l'erreur se produit toujours. Si ce n'est pas le cas, utilisez la fonction Annuler pour restaurer la section supprimée, puis essayez de supprimer l'autre moitié. Répétez le processus de réduction de moitié sur la section qui échoue, ou choisissez-en une au hasard si les deux le font. Continuez ainsi jusqu'à ce que vous trouviez la plus petite section possible, idéalement avec une seule citation, qui doit être présente pour que le problème se produise. Supprimez les citations isolées du document d'origine et le problème devrait disparaître (à moins qu'il n'y ait plusieurs citations brisées, auquel cas vous devrez répéter le processus). À moins que l'erreur ne se produise encore si vous effacez complètement le contenu du document, **cette dernière étape permettra par définition d'identifier le problème.**

Si vous êtes face à un document cassé, veuillez créer un nouveau fil de discussion sur le [forum Zotero](https://www.zotero.org/forum) afin que nous puissions tenter de résoudre le problème. N'oubliez pas d'inclure [un Ridentifiant de rapport](../reporting_problems.md#1-fournissez-un-report-id-identifiant-de-rapport) de Zotero, la version de votre système d'exploitation et de LibreOffice, ainsi que les étapes que vous avez tentées pour corriger l'erreur. Vous devez également envoyer l'extrait de document de l'étape 11 et un lien vers votre fil de discussion sur le forum à [support@zotero.org](mailto:support@zotero.org) afin que nous puissions essayer de reproduire le problème.

