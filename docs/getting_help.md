# Obtenir de l'aide

*Consulter cette page dans la documentation officielle de Zotero : [Getting Help](https://www.zotero.org/support/getting_help) - dernière mise à jour de la traduction : 2023-05-26*
{ data-search-exclude }

Besoin d'aide avec Zotero? Afin de vous aider le plus rapidement possible à résoudre votre problème, suivez les étapes ci-dessous.

### Étape 1 : Mettez à jour Zotero

[Mettez à jour Zotero](./installation.md) si vous n'utilisez pas [la dernière version](https://www.zotero.org/support/changelog), car votre problème a peut-être déjà été résolu dans la version la plus récente de Zotero. Vous pouvez vérifier votre version en sélectionnant "À propos de Zotero" dans le menu "Zotero" (Mac) ou dans le menu "Aide" de Zotero (Windows/Linux).

### Étape 2 : Consultez les pages de dépannage

Si vous avez une question générale à propos de Zotero, consultez la base de connaissance ou [la FAQ sur le site Zotero](https://www.zotero.org/support/frequently_asked_questions).

Si vous rencontrez un problème, les pages de dépannage dédiées suivantes peuvent vous aider.

-   [Problèmes pour installer Zotero](./installation.md)
-   [Problèmes avec vos données Zotero](./zotero_data.md) (par ex., bibliothèque manquante, restauration depuis une sauvegarde)
-   [Problèmes pour enregistrer des documents à partir de sites web](./troubleshooting_translator_issues.md)
-   [Problèmes avec les extensions de traitement de texte](https://www.zotero.org/support/word_processor_plugin_troubleshooting)
-   [Problèmes avec la synchronisation des données](./kb/changes_not_syncing.md) 
-   [Problèmes avec la synchronisation des fichiers](./kb/files_not_syncing.md) 
-   [Problèmes avec la gestion des fichiers](./kb/file_handling_issues.md) (par ex., PDF s'ouvrant dans le mauvais logiciel)

### Étape 3 : Publiez un message sur les forums (Vraiment! Cela fonctionne!)

Si vous ne parvenez pas à trouver la réponse à votre question dans la documentation, publiez un message sur les forums Zotero, où vous obtiendrez un support rapide et expert directement de la part des développeurs de Zotero, ainsi que de la part de membres de longue date de la communauté. Consultez la page [Comment fonctionne le support Zotero](./zotero_support.md) pour plus d'informations sur la manière dont cela nous permet d'apporter le meilleur support possible.

En général, veuillez lancer un nouveau fil de discussion pour votre problème. Il est préférable de lancer un nouveau fil de discussion plutôt que d'alimenter une discussion avec un problème qui peut être sans relation avec le sujet discuté. Les développeurs de Zotero ou les bénévoles qui répondent sur les forums peuvent vous indiquer un fil de discussion pertinent s'il en existe un.

Si vous êtes nouveau sur les forums Zotero, veuillez lire les [lignes de conduite sur les forums Zotero](./forum_guidelines.md) et les [procédures de signalement des problèmes](./reporting_problems.md) avant de publier un message.

**Note:** Vous devez [créer un compte Zotero](https://forums.zotero.org/user/register/) et vous identifier pour publier des messages sur les forums Zotero. Vous pouvez choisir un nom d'utilisateur différent pour les forums à partir des [paramètres de votre compte](https://forums.zotero.org/user/settings/account).

<p style="font-size: 14px; font-weight: bold"><a href="https://forums.zotero.org">Aller sur les forums Zotero</a></p>
