# Renommage des fichiers

*Consulter cette page dans la documentation officielle de Zotero : [File Renaming](https://www.zotero.org/support/kb/annotations_in_database) - dernière mise à jour de la traduction : 2023-08-24*
{ data-search-exclude }

Zotero renomme automatiquement les PDF et autres fichiers enregistrés dans votre bibliothèque en fonction des détails bibliographiques de leur document parent (titre, auteur, etc.), ce qui vous évite d'avoir à trier des piles de fichiers aux noms aléatoires ou à renommer manuellement chaque nouveau fichier selon le format de votre choix.

## Comportement de renommage

Zotero renomme toujours les fichiers enregistrés depuis le web, via le connecteur Zotero ou les fonctionnalités "Ajouter un document par son identifiant" et "Trouver un PDF disponible".

Par défaut, Zotero renomme également les [fichiers joints](./attaching_files.md#fichiers-joints-et-fichiers-liés) que vous ajoutez aux documents en tant que première pièce jointe enfant, ainsi que les fichiers pour lesquels il réussit à [récupérer les métadonnées](./retrieve_pdf_metadata.md). Vous pouvez désactiver cette fonction en décochant l'option "Renommer automatiquement les pièces jointes en utilisant les métadonnées parentes" dans le volet "Générales" des paramètres de Zotero. Si un document a déjà une pièce jointe, les pièces jointes additionnelles ne seront pas renommées automatiquement, afin d'éviter de modifier les noms de fichiers des fichiers supplémentaires.

Les fichiers liés ne sont pas automatiquement renommés, mais vous pouvez activer l'option "Renommer les fichiers liés" dans le volet "Générales" des paramètres de Zotero pour que le renommage s'applique à eux également.

## Titre de pièce jointe vs. nom de fichier

Les pièces jointes ont deux noms distincts : le titre de la pièce jointe affiché dans la liste des documents et le nom du fichier sur le disque. Dans la liste des documents, la ligne du document parent affiche déjà des métadonnées telles que le titre et les auteurs, il n'est donc pas nécessaire de les répéter en montrant le nom du fichier en dessous, et le titre de la pièce jointe contient souvent des informations complémentaires sur la source du fichier (par exemple, "Version acceptée" ou "Version soumise" pour les [fichiers en libre accès](https://zotero.hypotheses.org/2130)). Vous pouvez voir le nom du fichier soit dans le volet de droite en cliquant sur la pièce jointe, soit dans le système de fichiers en cliquant avec le bouton droit de la souris → Localiser le fichier.

## Personnaliser le format des noms de fichiers

Par défaut, Zotero nomme les fichiers d'après le créateur (1-2 auteurs ou éditeurs), l'année et le titre du document parent :

```
Lee et al - 2023 - The First Room-Temperature Ambient-Pressure Superconductor.pdf
```

Si Zotero a toujours renommé les fichiers automatiquement, Zotero 7, actuellement en [version bêta](https://forums.zotero.org/discussion/105094/announcing-the-zotero-7-beta/p1), introduit une nouvelle syntaxe puissante pour personnaliser les noms de fichiers. Le format par défaut peut être personnalisé à partir du panneau "Générales" des paramètres de Zotero.

Voici la chaîne de caractères du modèle par défaut :

```
{{ firstCreator suffix=" - " }}{{ year suffix=" - " }}{{ title truncate="100" }}
```

Les variables et paramètres suivants sont pris en charge.

### Variables

| Variable  | Description |
| :--------------- |:---------------|
| `authors` |   Principaux créateurs du document parent ; en fonction du type de document, il peut s'agir d'auteurs ou d'artistes mais non d'éditeurs ou autres contributeurs. |  
| `editors`  |  Editeurs du document parent.   |  
| `creators`  |  Tous les créateurs du document parent.   |  
| `firstCreator`  |  Créateur du document parent  (1-2 auteurs ou éditeurs).   |  
| `itemType`  |  Type du document parent.   |  
| `year`  |  Année, extraite du champ date du document parent.   |  
| N'importe quel champ du document  |  La liste complète des champs est disponible en bas de cette page.   |  

### Paramètres

| Paramètre  | Variables | Valeur par défaut  | Description |
| :--------------- |:---------------|:--------------- |:---------------|
| `truncate`  | Toutes |   | Tronque la valeur de la variable à un nombre fixe de caractères : par exemple `{{ title truncate="20" }}` sera remplacé par les 20 premiers caractères du titre du document parent. La troncature intervient après l'application de tous les autres paramètres, à l'exception de `prefix`, `suffix` et `case`. |
| `prefix`  | Toutes |   | Préfixe la variable avec le(s) caractère(s) donné(s) : par exemple `{{ title prefix="titre" }}` sera remplacé par le mot "titre" suivi du titre du document parent. Si la variable est vide (par exemple, le titre du document parent est vide), l'ensemble de l'expression, y compris le préfixe, est ignoré. |
| `suffix`  | Toutes |   | Ajoute à la fin de la variable le(s) caractère(s) donné(s) : par exemple `{{ title suffix="!" }}` sera remplacé par le titre du document parent suivi d'un point d'exclamation. Si la variable est vide (par exemple, le titre du document parent est vide), l'ensemble de l'expression, y compris le suffixe, est ignoré. |
| `case`  | Toutes |   | Convertit la casse d'une variable ; les valeurs suivantes sont acceptées : `upper`, `lower`, `sentence`, `title`, `hyphen`, `snake`, `camel`. Par exemple, {{ title case="snake" }} donnera titre_rendu_comme_ceci dans le nom du fichier. |
| `max`  | `authors`, `editors`, `creators` |   |  Limite le nombre de créateurs à utiliser : par exemple `{{ editors max="1" }}` sera remplacé par le premier éditeur du document parent.  |
| `name`  | `authors`, `editors`, `creators` | `family`  | Personnalise la façon dont le nom du créateur apparaît dans le nom du fichier, avec l'une des options suivantes. `family-given` utilise le nom complet du créateur, en commençant par son nom de famille ; `given-family` utilise également le nom complet mais inverse l'ordre ; et les options `given` et `family` n'utilisent qu'une partie du nom du créateur du document parent. |
| `name-part-separator`  | `authors`, `editors`, `creators` | ` ` (espace simple unique)  | Définit les caractères à utiliser pour séparer le prénom et le nom de famille ; il est particulièrement utile lorsqu'il est combiné avec `initialize`. |
| `join`  | `authors`, `editors`, `creators` |   `,` | Définit les caractères à utiliser pour séparer les créateurs consécutifs. |
| `initialize`  | `authors`, `editors`, `creators` |   | Permet l'utilisation d'initiales pour une partie ou la totalité du nom des créateurs, avec l'une des options suivantes. `full` utilise les initiales pour la totalité du nom ; `given` et `family` n'utilisent les initiales que pour la partie du nom qu'ils désignent respectivement. L'ordre des parties du nom est contrôlé par le paramètre `name` et seules les parties incluses dans le paramètre `name` peuvent être converties en initiales. Par exemple, `{{ authors name="given-family" initialize="given" }` sera remplacé par une liste d'auteurs séparés par des virgules, où le prénom de chaque auteur est remplacé par une initiale, suivie d'un point et d'une espace (par exemple J. Smith, D. Jones). |
| `initialize-with`  | `authors`, `editors`, `creators` |  `.` | Contrôle le caractère ajouté à l'initiale, si la partie du nom a été réduite à l'initiale. |
| `localize`  | `itemType` |   | Permet d'utiliser ou non la valeur localisée de la variable pour le type de document : par exemple `{{ itemType localize="true" }}` sera remplacé par le type du document parent orthographié dans la langue utilisée par Zotero. |

### Exemples

Une année de publication, suivie d'une liste d'auteurs séparés par des tirets, suivie d'un titre tronqué à 30 caractères :

Modèle : `{{ year suffix="-" }}{{ authors name="family-given" initialize="given" initialize="-" join="-" suffix="-" case="hyphen" }}{ title truncate="30" case="trait d'union" }}`

Nom de fichier : `2023-lee-sukbae-kim-ji-hoon-kwon-young-wan-la-première-chambre-température-amb.pdf`

Tout ce qui n'est pas inclus dans une accolade `{{` est copié littéralement dans le nom de fichier :

Modèle : `{{ itemType localize="true" }} de {{ année }} par {{ auteurs max="1" name="given-family" initialize="given" }}`

Nom de fichier : `Preprint de 2023 par S. Lee.pdf`

Les modèles prennent également en charge les conditions, certaines parties du modèle peuvent être incluses ou exclues en utilisant une combinaison de `if`, `elseif`, `else`. La condition doit se terminer par `endif`. Le modèle ci-dessous utilisera le DOI pour les articles de revue et les prépublications, l'ISBN pour les livres et le titre pour tout autre type de document.

`{{ if itemType == "book" }}{{ISBN}}{ elseif itemType == "preprint" }}{ DOI }}{ elseif itemType == "journalArticle" }}{ DOI }}{ else }}{ title }}{ endif }}`

### Liste complète des champs

`title`

`abstractNote`

`artworkMedium`

`artworkSize`

`date`

`language`

`shortTitle`

`archive`

`archiveLocation`

`libraryCatalog`

`callNumber`

`url`

`accessDate`

`rights`

`extra`

`audioRecordingFormat`

`seriesTitle`

`volume`

`numberOfVolumes`

`place`

`label`

`runningTime`

`ISBN`

`billNumber`

`code`

`codeVolume`

`section`

`codePages`

`legislativeBody`

`session`

`history`

`blogTitle`

`websiteType`

`series`

`seriesNumber`

`edition`

`publisher`

`numPages`

`bookTitle`

`pages`

`caseName`

`court`

`dateDecided`

`docketNumber`

`reporter`

`reporterVolume`

`firstPage`

`versionNumber`

`system`

`company`

`programmingLanguage`

`proceedingsTitle`

`conferenceName`

`DOI`

`identifier`

`type`

`repository`

`repositoryLocation`

`format`

`citationKey`

`dictionaryTitle`

`subject`

`encyclopediaTitle`

`distributor`

`genre`

`videoRecordingFormat`

`forumTitle`

`postType`

`committee`

`documentNumber`

`interviewMedium`

`publicationTitle`

`issue`

`seriesText`

`journalAbbreviation`

`ISSN`

`letterType`

`manuscriptType`

`mapType`

`scale`

`country`

`assignee`

`issuingAuthority`

`patentNumber`

`filingDate`

`applicationNumber`

`priorityNumbers`

`issueDate`

`references`

`legalStatus`

`episodeNumber`

`audioFileType`

`archiveID`

`presentationType`

`meetingName`

`programTitle`

`network`

`reportNumber`

`reportType`

`institution`

`organization`

`number`

`status`

`nameOfAct`

`codeNumber`

`publicLawNumber`

`dateEnacted`

`thesisType`

`university`

`studio`

`websiteTitle`
