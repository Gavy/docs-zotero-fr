# FAQ pour Zotero Institution Storage

*Consulter cette page dans la documentation officielle de Zotero : [Zotero Institution Storage FAQ](https://www.zotero.org/support/storage_institutions_faq) - dernière mise à jour de la traduction : 2023-06-09*
{ data-search-exclude }

## Quelle est la différence entre les abonnements Lab et Institution ?

Les abonnements Zotero Lab et Zotero Institution permettent tous deux aux membres de votre organisation de disposer d'un espace de stockage personnel et collectif illimité.

Les abonnements Zotero Institution sont conçus pour les grandes organisations, dans lesquelles un nombre important d'utilisateurs sont susceptibles d'utiliser Zotero. Les abonnements Zotero Institution fournissent automatiquement un stockage illimité à tout utilisateur de Zotero disposant d'une adresse de courriel vérifiée du domaine de votre institution, sans qu'il soit nécessaire de gérer des utilisateurs spécifiques. Le prix est fondé sur le nombre total de personnels ou d'étudiants en équivalent temps plein (ETP) de votre organisation. Le prix est très réduit par rapport à nos autres offres, car nous savons que seule une minorité de personnes au sein d'une grande institution seront des utilisateurs de Zotero.

Les abonnements à Zotero Lab sont plus adaptés lorsque vous avez un petit nombre d'utilisateurs dont vous savez qu'ils utiliseront Zotero. Les abonnements Zotero Lab sont gérés à l'aide d'une simple interface pour maintenir la liste des adresses électroniques spécifiques qui doivent bénéficier de l'espace de stockage souscrit.

## Pourquoi notre université devrait-elle s'abonner à Zotero Institution ?

Nous expliquons [sur cette page pourquoi nous pensons que les gens devraient choisir Zotero plutôt que d'autres outils similaires](https://www.zotero.org/why) - [consulter la traduction de cette page en français](https://zotero.hypotheses.org/1998). Un abonnement Zotero Institution permet à tous les membres de votre institution d'utiliser Zotero sans limitations, ce qui aide chacun à mieux gérer sa recherche et encourage une plus grande collaboration via les bibliothèques de groupe. Enfin, outre la valeur ajoutée pour vos utilisateurs, la souscription d'un abonnement Zotero Institution permet de financer le développement, la maintenance, le support et les coûts d'infrastructure du logiciel et des services gratuits de Zotero, ce qui profite à la communauté universitaire et de recherche mondiale, y compris à de nombreuses personnes pour lesquelles les outils commerciaux sont hors de portée.


## Comment trouver l'interface de gestion de mon abonnement Zotero Lab ?

Lorsque vous souscrivez votre abonnement Lab pour la première fois, vous voyez un lien pour gérer l'abonnement.

Dès lors, lorsque vous êtes connecté au compte Zotero qui gère un abonnement Zotero Lab, vous aurez un lien dans vos [paramètres de stockage](https://www.zotero.org/settings/storage) pour "Gérer le stockage pour <Nom du Lab>". Ce lien vous amènera à l'interface de gestion présentée ci-dessous, où vous pourrez gérer la liste des courriels qui doivent bénéficier de l'espace de stockage souscrit, changer le nom de votre institution, et enfin renouveler votre abonnement lorsqu'il expire.

![Capture d'écran de l'interface de gestion de Zotero Lab](./images/zotero-lab-management-screenshot.png){class="img500px"}

## Les utilisateurs doivent-ils enregistrer de nouveaux comptes institutionnels Zotero ? Les utilisateurs peuvent-ils convertir leurs comptes existants ? Que se passe-t-il lorsque des utilisateurs quittent l'institution?

Tous les comptes utilisateurs de Zotero sont enregistrés de la même manière sur zotero.org. Les utilisateurs peuvent utiliser les comptes Zotero qu'ils possèdent déjà, ainsi que conserver le compte Zotero et leurs données après la fin de votre abonnement ou lorsqu'ils quittent votre institution.

Les utilisateurs peuvent ajouter des adresses électroniques supplémentaires à leur compte Zotero à tout moment à partir des [paramètres de leur compte](https://www.zotero.org/settings/account). Si l'une de leurs adresses électroniques vérifiées est couverte par un abonnement Zotero Institution, ils verront le stockage illimité apparaître dans leurs [paramètres de stockage](https://www.zotero.org/settings/storage).

## Comment supprimer de notre abonnement les utilisateurs qui ont quitté l'institution ?

Il n'est pas nécessaire pour une organisation de supprimer des utilisateurs. Étant donné que les abonnements Zotero Institution sont facturés sur la base du nombre total d'ETP de l'organisation, il n'y a pas de coût pour l'organisation à laisser des utilisateurs couverts jusqu'à ce qu'ils soient supprimés.

Nous pouvons périodiquement demander aux utilisateurs bénéficiant du stockage illimité d'un abonnement Zotero Institution de revérifier leur adresse électronique.

## L'abonnement Zotero Institution nécessite-t-il une gestion ou une administration ?

Aucune gestion n'est requise pour les abonnements Zotero Institution. Une fois l'abonnement activé, tout utilisateur de Zotero dont le compte est associé à une adresse électronique éligible verra qu'il dispose d'un espace de stockage illimité fourni par votre organisation dans ses paramètres de stockage sur zotero.org.

Sauf indication contraire, tous les sous-domaines sont automatiquement inclus dans votre domaine. Par exemple, student1@dept.university.edu est inclus dans le domaine de messagerie university.edu.

Si l'un de vos utilisateurs s'est inscrit avec une adresse électronique différente, il peut ajouter son adresse institutionnelle à son compte lorsqu'il se connecte à https://www.zotero.org/settings/account. L'espace de stockage illimité sera alors appliqué à son compte.

## Pouvez-vous fournir une liste des utilisateurs de Zotero dans notre institution ?

Chaque compte Zotero appartient à l'utilisateur qui l'a enregistré et est couvert par nos conditions d'utilisation et notre politique de confidentialité, même si son stockage est fourni par un abonnement Zotero Lab ou Zotero Institution. Conformément à notre politique de confidentialité, nous ne partageons aucune information sur des utilisateurs spécifiques.

## Quelles sont les options de paiement pour les abonnements Zotero Lab et Zotero Institution ? Les renouvellements sont-ils automatiques ?

Les abonnements Zotero Lab et Zotero Institution ne sont pas renouvelés automatiquement.

Les abonnements Zotero Lab peuvent être renouvelés à partir de l'interface de gestion en utilisant une carte de crédit ou de débit. Si vous avez un abonnement Zotero Lab et que vous ne pouvez pas payer par carte, vous pouvez envoyer un courriel à [storage@zotero.org](mailto:storage@zotero.org) pour demander une facture qui pourra être payée par virement bancaire.

Pour des raisons de commodité, les abonnés Zotero Institution reçoivent chaque année une facture qui peut être payée par carte ou par virement bancaire. Cette facture n'oblige pas votre organisation à poursuivre son abonnement.

## Quel type de support est-il fourni pour les abonnements Zotero Institution?

Les abonnements Zotero Institution sont conçus pour être transparents et ne nécessitent aucune gestion. Nous pouvons toujours répondre aux questions concernant votre abonnement par courriel à [storage@zotero.org](mailto:storage@zotero.org). En ce qui concerne le support Zotero en général, nous fournissons le même [niveau de support de classe mondiale](./zotero_support.md) à tous les utilisateurs.

## Mon université dispose-t-elle d'un abonnement Zotero Institution ? Comment puis-je obtenir un espace de stockage illimité ?

Si votre institution est déjà abonnée à Zotero, il vous suffit d'ajouter votre adresse électronique institutionnelle à votre compte Zotero à partir des [paramètres de votre compte](https://www.zotero.org/settings/account). Si votre institution dispose déjà d'un abonnement, vous verrez que vous disposez d'un stockage illimité dans vos [paramètres de stockage](https://www.zotero.org/settings/storage), avec une note indiquant que "<nom de l'institution> fournit un stockage illimité pour <adresse électronique éligible>".

Sinon, vous pouvez faire savoir à la bibliothèque de votre université ou à tout autre contact approprié que vous souhaiteriez bénéficier d'un abonnement et qu'ils peuvent nous contacter s'ils sont intéressés.

## Qu'advient-il de l'espace de stockage d'un utilisateur lorsque son abonnement prend fin ou qu'il quitte l'institution ?

Un utilisateur de Zotero peut conserver son compte Zotero même si son affiliation institutionnelle change. Les utilisateurs doivent s'assurer qu'ils ont une adresse électronique personnelle associée à leur compte Zotero, afin de pouvoir accéder à leur compte Zotero même s'ils n'ont plus accès à l'adresse électronique de leur institution. Il est toujours possible d'ajouter des adresses électroniques supplémentaires à partir des [paramètres de son compte Zotero](https://www.zotero.org/settings/account). 

Si un utilisateur n'est plus couvert par l'abonnement de son institution ou que cet abonnement expire, son abonnement de stockage revient au niveau de son abonnement de stockage personnel antérieur ou au niveau du stockage gratuit. L'utilisateur peut soit acheter un abonnement de stockage personnel pour répondre à ses besoins, soit rester au niveau gratuit. 

Un utilisateur de Zotero peut toujours télécharger tous ses fichiers sur son ordinateur afin d'en conserver l'accès sur son appareil, en paramétrant son application Zotero pour "Télécharger les fichiers lors de la synchronisation" dans les [préférences de synchronisation](./preferences_sync.md) et en s'assurant qu'une synchronisation se termine sans erreur. Après avoir téléchargé tous ses fichiers, si un utilisateur ne souhaite pas souscrire à un abonnement de stockage pour couvrir le stockage en ligne, il peut désactiver la synchronisation des fichiers dans ses préférences de synchronisation. Cela évite d'avoir des erreurs en permanence, tout en continuant à synchroniser les métadonnées gratuitement.