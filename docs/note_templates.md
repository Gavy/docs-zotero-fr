# Modèles de note

*Consulter cette page dans la documentation officielle de Zotero : [Note Templates](https://www.zotero.org/support/note_templates) - dernière mise à jour de la traduction : 2023-02-22*
{ data-search-exclude }

## Annotations

Vous pouvez utiliser des modèles de note pour personnaliser la façon dont les annotations PDF sont [ajoutées aux notes](./pdf_reader.md#ajout-dannotations-aux-notes). Pour afficher les modèles disponibles, allez dans le volet "Avancées" des préférences de Zotero, ouvrez l'"Editeur de configuration" et recherchez `annotations.noteTemplates`. Il existe actuellement trois modèles disponibles : pour les annotations de surlignage (highlight), pour les annotations de notes (note) et pour le titre des notes créées à partir de toutes les annotations d'un document (title).

Les modèles prennent en charge le langage HTML basique, avec des variables entre crochets. Voici le modèle par défaut pour les surlignages : 

    <p>{{highlight}} {{citation}} {{comment}}</p>

Vous pouvez constater que, par défaut, les annotations de surlignage sont ajoutées sous la forme d'un seul paragraphe, avec le texte surligné suivi de la citation et du commentaire évenutuel. Des guillemets sont automatiquement ajoutés autour du passage surligné.

Si vous préférez que le texte mis en évidence soit placé dans un bloc de citation, il suffit de modifier le code ainsi :

    <blockquote>{{highlight}}</blockquote><p>{{citation}} {{comment}}</p>

### Expressions conditionnelles

Les modèles prennent également en charge les expressions conditionnelles. Plutôt que de combiner la citation et le commentaire dans un seul paragraphe comme dans l'exemple précédent, vous pourriez vouloir créer un paragraphe séparé pour le commentaire, mais seulement si un commentaire existe réellement. Vous pouvez tester si une variable est définie avec un simple `if` :

    <blockquote>{{highlight}}</blockquote><p>{{citation}}</p>{{if comment}}<p>{{comment}}</p>{{endif}}

Les expressions conditionnelles peuvent également être utilisées pour tester des valeurs spécifiques. Ici, le texte surligné en rouge devient un en-tête sans guillemets, le texte surligné en bleu devient une citation en retrait, et tous les autres textes surlignés sont compilés en un seul paragraphe :

    {{if color == '#ff6666'}}
        <h2>{{highlight quotes='false'}}</h2>
    {{elseif color == '#2ea8e5'}}
        {{if comment}}<p>{{comment}}:</p>{{endif}}<blockquote>{{highlight}}</blockquote><p>{{citation}}</p>
    {{else}}
        <p>{{highlight}} {{citation}} {{comment}}{{if tags}} #{{tags join=' #'}}{{endif}}</p>
    {{endif}}


### Variables

Voici les variables disponibles et les paramètres qu'elles supportent :

* `highlight`
    
    * `quotes`
        
        * vide: met le texte surligné entre guillemets, sauf s'il est placée à l'intérieur d'un bloc de citations (blockquote).
            
        * “true” : ajoute des guillemets dans tous les cas
            
        * “false” : n'ajoute jamais de guillemets. Le passage surligné doit être placé dans un bloc de citations (blockquote) pour rester une annotation active.
            
* `citation`
    
* `comment`
    
* `color` — jaune: '#ffd400', rouge: '#ff6666', vert: '#5fb236', bleu: '#2ea8e5', violet: '#a28ae5' , magenta: '#​e56eee',​ orange: '#​f19837',​ gray: '#​aaaaaa'
    
* `tags`
    
    * `join` — à utiliser pour ajouter les marqueurs
        

(Notez que `color` est destiné à être utilisé dans les expressions conditionnelles. Les couleurs des annotations peuvent être [activées et désactivées](./pdf_reader.md#afficher-les-couleurs-des-annotations) à partir du menu d'édition de la note. Une prochaine version ajoutera une préférence permettant de contrôler si les couleurs sont affichées par défaut.)
