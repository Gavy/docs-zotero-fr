# FAQ pour Zotero Storage

*Consulter cette page dans la documentation officielle de Zotero : [Zotero Storage FAQ](https://www.zotero.org/support/storage_faq) - dernière mise à jour de la traduction : 2023-05-11*
{ data-search-exclude }

## Qu'est-ce que Zotero Storage ?

Zotero Storage fournit un espace de stockage en ligne pour vos fichiers Zotero, vous permettant de :

* synchroniser des PDF, des images, des captures de page web et autres fichiers entre tous vos ordinateurs,
* partager les fichiers joints dans vos bibliothèques de groupe Zotero,
* accéder aux fichiers via votre bibliothèque en ligne sur zotero.org.

Vous pouvez toujours enregistrer un nombre illimité de fichiers dans votre bibliothèque Zotero locale, avec ou sans Zotero Storage.

## Comment souscrire à Zotero Storage ?

Il vous suffit de vous rendre sur le profil de votre compte Zotero et de [sélectionner une offre de stockage](https://www.zotero.org/settings/storage).

## Comment souscrire à Zotero Storage pour l'ensemble de mon laboratoire, de mon université ou de mon entreprise ?

[Zotero Lab et Zotero Institution](https://www.zotero.org/storage/institutions) permettent aux membres de votre organisation de disposer d'un espace de stockage personnel et collectif illimité.

## Comment puis-je payer pour Zotero Storage ?

Toutes les principales cartes de crédit sont acceptées pour le paiement. Les offres Zotero Lab et Zotero Institution peuvent également être payées par virement bancaire.

## Que faire si je n'ai pas de carte de crédit ou si ma carte de crédit ne fonctionne pas ?

Si vous n'avez pas de carte de crédit ou si vous rencontrez des problèmes après avoir essayé de payer via la [page de vos paramètres de stockage](https://www.zotero.org/settings/storage), nous pouvons vous facturer via PayPal. Veuillez envoyer un courriel à [storage@zotero.org](mailto:storage@zotero.org) en indiquant votre nom d'utilisateur Zotero et le volume de stockage que vous souhaitez souscrire (2 Go, 6 Go ou illimité).

## Les fichiers stockés dans mes groupes seront-ils librement accessibles aux autres membres de ces groupes ?

Oui, tous les groupes dont vous êtes propriétaire puiseront dans votre abonnement pour le stockage des fichiers joints. Tous les membres de ces groupes pourront accéder librement aux fichiers stockés.

## Comment répartir l'espace de stockage entre ma bibliothèque personnelle et mes groupes ?

Votre bibliothèque personnelle et les groupes dont vous êtes propriétaire puisent automatiquement leur espace de stockage dans votre abonnement. Les groupes dont vous n'êtes pas propriétaire puisent leur espace de stockage dans l'abonnement du propriétaire du groupe.

## Comment puis-je modifier mon offre de stockage actuelle ?

Vous pouvez changer l'offre de stockage à laquelle vous êtes actuellement abonné à tout moment à partir de vos [paramètres de stockage](https://www.zotero.org/settings/storage).

Lorsque vous passez à une nouvelle offre, vous n'êtes pas facturé immédiatement pour la nouvelle offre - votre date d'expiration est simplement ajustée au prorata du solde  de votre ancienne offre. Le premier prélèvement pour votre nouveau niveau d'abonnement sera effectué à votre nouvelle date d'expiration.

_Exemple : Nous sommes en février 2022, et il vous reste 6 mois sur un abonnement de 2 Go à 20 $, avec une date d'expiration en août 2022. (6 mois ÷ 12 mois) x 20 $ = 10 $ de valeur restante. Si vous passez à l'abonnement de 6 Go à 60 $, nous appliquons ces 10 $ à votre nouveau niveau d'abonnement pour fixer une nouvelle date d'expiration. (10 $ ÷ 60 $) * 12 mois = 2 mois, votre nouvelle date d'expiration sera donc dans deux mois, en avril. Vous n'avez payé que les 20 $ d'origine en août dernier, et vous serez facturé 60 $ pour la première fois en avril._

(La seule exception concerne le cas où il vous reste moins de deux semaines d'abonnement. Dans ce cas, le solde inutilisé est toujours appliqué à votre nouvel abonnement, mais nous vous facturerons la nouvelle offre immédiatement).

## Mon abonnement sera-t-il renouvelé automatiquement ?
Lorsque vous souscrivez un abonnement de stockage, vous pouvez choisir de le renouveler ou non automatiquement chaque année. Vous pouvez annuler le renouvellement automatique à tout moment à partir de vos [paramètres de stockage](https://www.zotero.org/settings/storage). Vous recevrez également un courriel de rappel lorsque votre abonnement arrivera à expiration ou sera sur le point d'être renouvelé automatiquement.

## Comment puis-je annuler mon abonnement ?

Si vous avez choisi de renouveler votre abonnement automatiquement, vous pouvez annuler le prochain renouvellement à partir de vos [paramètres de stockage](https://www.zotero.org/settings/storage). Comme indiqué dans les [conditions de service](https://www.zotero.org/support/terms/terms_of_service), les comptes payants ne sont pas remboursables. Nous appliquons cette politique parce que les coûts encourus de notre côté fluctuent considérablement en fonction des types d'usage individuels.
